<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/header__bg_3.jpg');">
            <div class="container">
                <h1>
                    <span>История</span>
                    <br>
                    <span>транзакций</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <?php include('inc/user_sidebar.inc.php') ?>
                    <!-- -->

                </div>
                <div class="main_content">

                    <div class="table_responsive">
                        <table class="table">
                            <tr>
                                <th>сумма</th>
                                <th>дата</th>
                                <th>статус</th>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_brown">++25$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_brown">Списано за заказ #32342</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_gray">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_gray">Резерв заказ #32342</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_gray">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_gray">Резерв заказ #32342</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_brown">++25$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_brown">Списано за заказ #32342</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_brown">++25$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_brown">Списано за заказ #32342</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                            <tr>
                                <td class="extra_bold"><span class="color_green">+356$</span></td>
                                <td>15.12.2018, 11:23</td>
                                <td><span class="color_green">Поступило | Visa #34242323</span></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
