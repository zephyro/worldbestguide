<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/guide_info__bg.jpg');">
            <div class="container">
                <h1>
                    <span>ЭТО БЫЛА УДИВИТЕЛЬНАЯ ПОЕЗДКА, КОТОРАЯ ИЗМЕНИЛА МОЙ ВЗГЛЯД НА ФРАНЦИЮ</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <?php include('inc/user_sidebar.inc.php') ?>
                    <!-- -->

                </div>
                <div class="main_content">

                    <h3 class="extra_bold">АЛЕКСАНДР</h3>
                    <div class="blog_date">16.11.2018, 11:32</div>
                    <div class="text_lead mb_60">
                        Мы приехали в самый жаркий месяц и мы сразу окунились в жизнь города, который со старых времен приподносит уникальные. Большое спасибо нашему гиду Александру Пушкову, который позволил нам окунуться в мир Парижа, который сделал наш день незабываемым.
                        То что мы создаем, дает нам возможность решать глобальные задачи в Мире.
                    </div>

                    <div class="content_image mb_30">
                        <img src="images/blog__image_01.jpg" class="img-fluid" alt="">
                    </div>

                    <div class="content_image mb_40">
                        <img src="images/blog__image_02.jpg" class="img-fluid" alt="">
                    </div>

                    <h2>Затерянные каналы Болоньи</h2>
                    <p>
                        Неумолимо приближается новый туристический сезон и Италия в этом году пустовать не будет, Венеция, Рим и Флоренция будут кишеть толпами туристов. Что же делать, если хочется уединения и романтики? Сайт GIDtravel.com предлагает подборку романтических мест Италии о которых никто не знает.
                    </p>

                    <div class="content_image mb_40">
                        <img src="images/blog__image_03.jpg" class="img-fluid" alt="">
                    </div>

                    <div class="blog_like">
                        <a class="blog_like__button" href="#"></a>
                        <span class="blog_like__text">+256</span>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
