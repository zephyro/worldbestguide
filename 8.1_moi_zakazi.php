<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/header__bg_3.jpg');">
            <div class="container">
                <h1>
                    <span>Мои</span>
                    <br>
                    <span>заказы</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <?php include('inc/user_sidebar.inc.php') ?>
                    <!-- -->

                </div>
                <div class="main_content">

                    <h3>новые запросы</h3>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th>название</th>
                                <th>дата</th>
                                <th class="text-center">статус</th>
                                <th class="text-center">сумма</th>
                                <th class="text-center">сообщение</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                        </table>
                    </div>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th>название</th>
                                <th>дата</th>
                                <th class="text-center">статус</th>
                                <th class="text-center">сумма</th>
                                <th class="text-center">сообщение</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
