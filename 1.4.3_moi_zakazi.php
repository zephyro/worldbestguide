<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_two" style="background-image: url('img/header__bg_4.jpg');">
            <div class="container">
                <h1><span>Мои заказы</span></h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row main_row_wrap">

                <div class="main_sidebar">

                    <div class="user_sidebar">

                        <div class="sidebar_photo">
                            <div class="sidebar_photo__item">
                                <div class="sidebar_photo__empty">

                                    <label class="sidebar_photo__upload">
                                        <input type="file" name="photo" value="">
                                        <span>загрузить<br/>ФОТО</span>
                                    </label>

                                </div>
                                <span>aLEX PUSHKOV</span>
                            </div>
                        </div>


                        <div class="sidenav">
                            <ul class="sidenav__menu">
                                <li><a href="#">МОИ ЗАКАЗЫ (5) <span class="sidenav__menu_value">+3</span></a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Дополнительно</div>

                            <ul class="sidenav__menu">
                                <li><a href="#">Добавить статью</a></li>
                                <li><a href="#">МОИ СТАТЬИ</a></li>

                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Настройки</div>

                            <ul class="sidenav__menu">
                                <li><a href="#">Мои настройки</a></li>
                                <li><a href="#" class="sidenav__menu_exit">ВЫХОД</a></li>
                            </ul>
                        </div>

                    </div>

                </div>

                <div class="main_content">
                    <h3>Мои заказы</h3>
                    <div class="table_responsive">
                        <table class="table">
                            <tr>
                                <th>название</th>
                                <th class="text-center">сообщение</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"></a><a href="#" class="blue_link">Ответить</a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"></a><a href="#" class="blue_link">Ответить</a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"></a><a href="#" class="blue_link">Ответить</a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td class="text-center"><a class="comments_value" href="#">+2</a></td>
                                <td class="text-center"><a class="text-uppercase" href="#"></a><a href="#" class="blue_link">Ответить</a></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
