<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="page_heading_wrap">

                <div class="page_heading page_heading_one" style="background-image: url('img/search_top_bg.jpg');">
                    <div class="container">
                        <h1 contenteditable="true">
                            <span class="string_top_md">Интересные истории</span><br/>
                            <span class="string_bottom_sm">от путешественников и гидов</span>
                        </h1>
                    </div>
                </div>
            </div>


            <section class="main">
                <div class="container">
                    <div class="main_row">

                        <div class="main_sidebar">

                            <div class="sidebar_group">
                                <div class="sidebar_heading">Все виды туров</div>
                                <ul class="tours_nav">
                                    <li>
                                        <a href="#">
                                            <span>Водный тур, Рафтинг</span>
                                            <i class="tours_nav_icon_01">
                                                <svg class="ico-svg" viewBox="0 0 21 22" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_01" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Дайвинг</span>
                                            <i class="tours_nav_icon_02">
                                                <svg class="ico-svg" viewBox="0 0 22 23" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_02" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Развлекательный</span>
                                            <i class="tours_nav_icon_03">
                                                <svg class="ico-svg" viewBox="0 0 18 19" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_03" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>винный</span>
                                            <i class="tours_nav_icon_04">
                                                <svg class="ico-svg" viewBox="0 0 20 17" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_04" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Джиппинг</span>
                                            <i class="tours_nav_icon_05">
                                                <svg class="ico-svg" viewBox="0 0 26 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_05" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Железнодорожный</span>
                                            <i class="tours_nav_icon_06">
                                                <svg class="ico-svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>гастрономический ТУР</span>
                                            <i class="tours_nav_icon_07">
                                                <svg class="ico-svg" viewBox="0 0 21 20" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Бизнес тур</span>
                                            <i class="tours_nav_icon_08">
                                                <svg class="ico-svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_08" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Temple stay</span>
                                            <i class="tours_nav_icon_09">
                                                <svg class="ico-svg" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_09" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>лечение за рубежом</span>
                                            <i class="tours_nav_icon_10">
                                                <svg class="ico-svg" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_10" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Исторические, Обзорные</span>
                                            <i class="tours_nav_icon_11">
                                                <svg class="ico-svg" viewBox="0 0 17 21" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_11" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Охота и рыбалка</span>
                                            <i class="tours_nav_icon_12">
                                                <svg class="ico-svg" viewBox="0 0 29 23" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_12" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Пешеходная экскурсия</span>
                                            <i class="tours_nav_icon_13">
                                                <svg class="ico-svg" viewBox="0 0 11 27" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_13" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Приключения и экстрим</span>
                                            <i class="tours_nav_icon_14">
                                                <svg class="ico-svg" viewBox="0 0 19 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_14" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Сафари и треккинг</span>
                                            <i class="tours_nav_icon_15">
                                                <svg class="ico-svg" viewBox="0 0 20 16" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_15" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Шопинг тур</span>
                                            <i class="tours_nav_icon_16">
                                                <svg class="ico-svg" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_16" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Экологический тур</span>
                                            <i class="tours_nav_icon_17">
                                                <svg class="ico-svg" viewBox="0 0 20 22" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_17" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Экспедиция</span>
                                            <i class="tours_nav_icon_18">
                                                <svg class="ico-svg" viewBox="0 0 23 14" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_18" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="main_content">

                            <div class="blog">
                                <div class="blog__col blog__col_large">
                                    <a href="#" class="blog__tile blog__tile_large">
                                        <div class="blog__tile_image">
                                            <div class="blog__tile_inner">
                                                <img src="images/direction__01.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <span class="blog__tile_name">Дайвинг тур от 150$</span>
                                        <div class="blog__tile_title">ДАЙВИНГ -<br>НЕЗАБЫВАЕМОЕ ПОГРУЖЕНИЕ ПОД ВОДУ</div>
                                        <div class="blog__tile_tags">Испания</div>
                                    </a>
                                </div>
                                <div class="blog__col">
                                    <a href="#" class="blog__tile">
                                        <div class="blog__tile_image">
                                            <div class="blog__tile_wrap">
                                                <img src="images/direction__02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <span class="blog__tile_name">Винный тур от 150$</span>
                                        <div class="blog__tile_text"><span>Попробуйте изысканные вина</span></div>
                                    </a>
                                </div>
                                <div class="blog__col">
                                    <a href="#" class="blog__tile">
                                        <div class="blog__tile_image">
                                            <img src="images/direction__03.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span class="blog__tile_name">Шопинг тур от 130$</span>
                                        <div class="blog__tile_text"><span>Лучший шопинг в моей жизни, хочу еще!</span></div>
                                    </a>
                                </div>
                                <div class="blog__col">
                                    <a href="#" class="blog__tile">
                                        <div class="blog__tile_image">
                                            <img src="images/direction__04.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span class="blog__tile_name">Гастрономический тур от 150$</span>
                                        <div class="blog__tile_text"><span>Попробуйте изысканные блюда</span></div>
                                    </a>
                                </div>
                                <div class="blog__col">
                                    <a href="#" class="blog__tile">
                                        <div class="blog__tile_image">
                                            <img src="images/direction__05.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span class="blog__tile_name">Бизнес тур от 150$</span>
                                        <div class="blog__tile_text"><span>Организуем бизнес тур</span></div>
                                    </a>
                                </div>
                            </div>

                            <div class="goods_view">
                                <a href="#" class="btn_xl btn_long">пОКАЗАТЬ ЕЩЕ</a>
                            </div>

                            <div class="pagination">
                                <a href="#" class="pagination__prev"></a>
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><span>...</span></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">8</a></li>
                                </ul>
                                <a href="#" class="pagination__next"></a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
