<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/header__bg_3.jpg');">
            <div class="container">
                <h1>
                    <span>Мои</span>
                    <br>
                    <span>статьи</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <?php include('inc/user_sidebar.inc.php') ?>
                    <!-- -->

                </div>
                <div class="main_content">

                    <h3>Мои статьи</h3>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th>название</th>
                                <th>дата</th>
                                <th class="text-center">статус</th>
                                <th class="text-center">Лайки</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">123</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Подробнее</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_purple font_semibold text-center">На модерации</td>
                                <td class="text-center">123</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Подробнее</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-uppercase color_green font_semibold text-center">НОВЫЙ</td>
                                <td class="text-center">123</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Подробнее</a></a></td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->


</body>
</html>
