$(".btn_modal").fancybox({
    'padding'    : 0
});



(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $('.page').toggleClass('nav_open');
    });

}());



// Форма поиска

$('.selection_radio').on('click touchstart', function(e){
    var place = $(this).find('input').val();
    $('.selection').find('.selection_input').val(place);
    $('.selection').find('.selection_input').focus();
});




var popular = new Swiper('.popular__slider', {
    loop: true,
    slidesPerView: 4,
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
    breakpoints: {
        1550: {
            slidesPerView: 4,
            spaceBetween: 20,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    }
});


// Слайдер с отзывами

var staff = new Swiper('.staff__slider', {
    loop: true,
    slidesPerView: 5,
    spaceBetween: 20,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
    breakpoints: {
        1550: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    }
});

$('.staff__user').on('click touchstart', function(e){
    e.preventDefault();
    var tab = $($(this).attr("href"));

    $('.staff').find('.staff__user_image').removeClass('active');
    $(this).find('.staff__user_image').addClass('active');

    $('.staff').find('.staff__item').removeClass('active');
    $('.staff').find(tab).addClass('active');
});


$(".form_select_style").select2({
    language: "ru"
});


$('.raty').raty({
    number: 5,
    starHalf      : 'fa fa-star-half-o',
    starOff       : 'fa fa-star-o',
    starOn        : 'fa fa-star',
    cancelOff     : 'fa fa-minus-square',
    cancelOn      : 'fa fa-check-square',
    score: function() {
        return $(this).attr('data-score');
    },
    readOnly: function() {
        return $(this).attr('data-readOnly');
    },
});


// form image

$('.form_image input[type="file"]').on('change', function(e) {
    var box = $(this).closest('.form_image');
    var str = $(this).val();

    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    box.find('span').text(filename);
});

// SVG IE11 support
svg4everybody();

// Слайдер с фото

var gallery = new Swiper('.tour_gallery__slider', {
    loop: true,
    slidesPerView: 3,
    spaceBetween: 16,
    navigation: {
        nextEl: '.gallery_button_next',
        prevEl: '.gallery_button_prev',
    },
    breakpoints: {
        1550: {
            slidesPerView: 3
        },
        1200: {
            slidesPerView: 2
        },
        992: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 1
        }
    }
});
