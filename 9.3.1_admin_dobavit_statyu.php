<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/header__bg_3.jpg');">
            <div class="container">
                <h1>
                    <span>рЕДАКТИРОВАТЬ</span>
                    <br>
                    <span>статью</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <div class="user_sidebar">

                        <div class="sidebar_photo">
                            <div class="sidebar_photo__item">
                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                <span>aLEX PUSHKOV</span>
                            </div>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Админ</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">Заказы <span class="color_green">56</span> | <span class="color_purple">56</span></a></li>
                                <li><a href="#">Транзакции</a></li>
                                <li><a href="#">Статьи</a></li>
                                <li><a href="#">Пользователи</a></li>
                                <li><a href="#">Гиды <span class="color_green">56</span> | <span class="color_purple">56</span></a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Основное</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">МОИ Заказы (5)</a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Дополнительно</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">МОИ Статьи (5)</a></li>
                                <li><a href="#">Добавить статью</a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Я Гид</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">Мой баланс $345 | <span class="color_purple">$112</span></a></li>
                                <li><a href="#">МОИ ЗАКАЗЫ (5) <span class="sidenav__menu_value">+3</span></a></li>
                                <li><a href="#">МОИ услуги (5)</a></li>
                                <li><a href="#">МОИ туры (15)</a></li>
                                <li><a href="#" class="sidenav__menu_exit">ВЫХОД</a></li>
                            </ul>
                        </div>

                    </div>
                    <!-- -->

                </div>
                <div class="main_content">

                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                            <h3 class="pt_10">СОЗДАНИЕ НОВОЙ СТАТЬИ</h3>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr flex_end">
                            <ul class="btn_group">
                                <li><a href="#" class="btn">разместить</a></li>
                                <li><a href="#" class="btn_red">удалить</a></li>
                            </ul>
                        </div>
                    </div>


                    <div class="form_group mb_30">
                        <label class="form_label">Название</label>
                        <input type="text" class="form_control" name="text" placeholder="Текст" value="">
                    </div>

                    <div class="form_group">
                        <label class="form_label">Главное фото 1920х1080</label>
                        <label class="form_image">
                            <input type="file" name="file" value="">
                            <span>загрузить фотографии</span>
                        </label>
                    </div>

                    <div class="form_group mb_30">
                        <label class="form_label">Вводный текст</label>
                        <textarea class="form_control" name="" placeholder="" rows="4"></textarea>
                    </div>

                    <h3 class="extra_bold">ВИДЫ ОТДЫХА</h3>

                    <ul class="form_list mb_25">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Водный тур, Рафтинг</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Дайвинг тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Детский тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Дегустация вин, напитков</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Джиппинг</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Железнодорожный тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Кулинарный тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Культурно-ИсторическиЙ тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Охота</span>
                            </label>
                        </li>


                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Пешеходная экскурсия</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Прогулка</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Приключения и экстрим</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Рыбалка</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Сафари</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Треккинг тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Тур по природе</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Ценителям искусства</span>
                            </label>
                        </li>


                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Частный тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Шопинг тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Экскурсия</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Экологический тур</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="check" value="">
                                <span>Экспедиция</span>
                            </label>
                        </li>
                    </ul>


                    <h3 class="extra_bold">КОНТЕНТ</h3>
                    <div class="form_group mb_50">
                        <div id="summernote"></div>
                    </div>

                    <button type="submit" class="btn">СОХРАНИТЬ</button>



                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
