<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_two" style="background-image: url('img/header__bg_4.jpg');">
            <div class="container">
                <h1><span>По Версалю не спеша</span></h1>
                <br/>
                <span class="page_heading__second">Знакомство с детищем Короля-Солнце и пикник в великолепном парке</span>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row main_row_wrap">
                <div class="main_sidebar">

                    <div class="main_sidebar">

                        <div class="user_sidebar">

                            <div class="sidebar_photo">
                                <div class="sidebar_photo__item">
                                    <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                    <span>aLEX PUSHKOV</span>
                                </div>
                            </div>


                            <div class="sidenav">
                                <ul class="sidenav__menu">
                                    <li><a href="#">МОИ ЗАКАЗЫ (5) <span class="sidenav__menu_value">+3</span></a></li>
                                </ul>
                            </div>

                            <div class="sidenav">
                                <div class="sidenav__title">Дополнительно</div>

                                <ul class="sidenav__menu">
                                    <li><a href="#">Добавить статью</a></li>
                                    <li><a href="#">МОИ СТАТЬИ</a></li>

                                </ul>
                            </div>

                            <div class="sidenav">
                                <div class="sidenav__title">Настройки</div>

                                <ul class="sidenav__menu">
                                    <li><a href="#">Мои настройки</a></li>
                                    <li><a href="#" class="sidenav__menu_exit">ВЫХОД</a></li>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="main_content">
                    <div class="main_content__row">

                        <div class="main_center">
                            <h3>Что вас ожидает</h3>
                            <p class="mb_30">История - Архитектура- Культура - Королевский парк</p>
                            <h5>Версаль великолепный и его истории</h5>
                            <p>Королевская резиденция — место, где вершились судьбоносные события. Вы пройдёте по центральной аллее и через Золотые ворота, побываете в Зале коронации и Тронном зале, насладитесь видом из опочивальни Короля-Солнце и разгадаете символизм архитектурного решения комнаты. Вас поразит легендарный Зеркальный зал, где время словно остановилось: здесь вы услышите </p>

                            <div class="mb_40"></div>

                            <div class="tour_info">

                                <div class="tour_info__item">
                                    <div class="tour_info__status">
                                        20 отзывов на экскурсии гида.<br/>
                                        Оценка 5 из 5
                                    </div>
                                    <div class="tour_info__image">
                                        <img src="img/tour__icon_01.svg" alt="">
                                    </div>
                                    <div class="tour_info__name">ПРОВЕРЕННЫЙ <br/>ГИД</div>
                                </div>

                                <div class="tour_info__item">
                                    <div class="tour_info__status">Напишите комментарий к заказу и обсудите детали</div>
                                    <div class="tour_info__image">
                                        <img src="img/tour__icon_02.svg" alt="">
                                    </div>
                                    <div class="tour_info__name">ЗАДАВАЙТЕ <br/>ВОПРОСЫ</div>
                                </div>

                                <div class="tour_info__item">
                                    <div class="tour_info__status">
                                        20 отзывов на экскурсии гида.<br/>
                                        Оценка 5 из 5
                                    </div>
                                    <div class="tour_info__image">
                                        <img src="img/tour__icon_03.svg" alt="">
                                    </div>
                                    <div class="tour_info__name">ГАРАНТИЯ<br/>ЛУЧШЕЙ ЦЕНЫ</div>
                                </div>

                                <div class="tour_info__item">
                                    <div class="tour_info__status">
                                        20 отзывов на экскурсии гида.<br/>
                                        Оценка 5 из 5
                                    </div>
                                    <div class="tour_info__image">
                                        <img src="img/tour__icon_04.svg" alt="">
                                    </div>
                                    <div class="tour_info__name">БЕСПЛАТНО<br/>ОБЩАЙТЕСЬ С ГИДОМ</div>
                                </div>

                            </div>

                            <h3>Галерея тура</h3>

                            <div class="tour_gallery mb_40">
                                <div class="tour_gallery__slider swiper-container">
                                    <div class="swiper-wrapper">

                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__01.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__02.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__02.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__03.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__03.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__01.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__02.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__02.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__03.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__03.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__01.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__02.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__02.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/tour/gallery__03.jpg" data-fancybox="gallery">
                                                <img src="images/tour/gallery__03.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>

                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="gallery_button_next"></div>
                                    <div class="gallery_button_prev"></div>
                                </div>
                            </div>

                            <h3>маршрут</h3>
                            <p class="text-uppercase mb_40"><strong>Париж - Версаль - Париж</strong></p>

                            <h3>Место встречи + время тура</h3>
                            <p class="mb_40">
                                Из Парижа в Версаль добираемся на общественном транспорте (удобный и бюджетный способ).<br/>
                                ПО БУДНЯМ НАЧАЛО ЭКСКУРСИИ В 13:00. ПО ВЫХОДНЫМ В 11:00.
                            </p>

                            <h3>требуется местная наличность</h3>
                            <p class="mb_40">
                                Входной билет во дворец — 18 евро с человека;<br/>
                                Входной билет в Версальский сад — 9 евро с человека;<br/>
                                Проезд на комфортабельном общественном транспорте (туда-обратно) — 8 евро с человека.<br/>
                                Экскурсию для вас проведу я или другой гид из нашей команды.<br/>
                            </p>

                            <h3>что включено в тур</h3>
                            <ul class="list_circle mb_40">
                                <li>Питание</li>
                                <li>Экскурсионное сопровождение</li>
                                <li>Транспорт</li>
                                <li>Пикник включён в стоимость экскурсии.</li>
                            </ul>

                            <h3>ДОПОЛНИТЕЛЬНЫЕ Услуги оказываемые гидом</h3>
                            <ul class="list_circle mb_40">
                                <li>Переводчик: английский, русский, французский</li>
                                <li>Встреча в аэропорту</li>
                                <li>Сопровождение на бизнес встречах</li>
                                <li>Произвольные экскурсии по городу</li>
                                <li>Переводчик: английский, русский, французский</li>
                                <li>Встреча в аэропорту</li>
                                <li>Сопровождение на бизнес встречах</li>
                                <li>Произвольные экскурсии по городу</li>
                            </ul>

                            <h3>транспорт</h3>
                            <div class="image_group mb_40">
                                <div class="image_group__item">
                                    <img src="images/transport.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="image_group__item">
                                    <img src="images/transport.jpg" class="img-fluid" alt="">
                                </div>
                            </div>

                            <h3>МЕню</h3>

                            <div class="tour_gallery mb_40">
                                <div class="tour_gallery__slider swiper-container">
                                    <div class="swiper-wrapper">

                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/food__01.jpg" data-fancybox="gallery">
                                                <img src="images/food__01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/food__02.jpg" data-fancybox="gallery">
                                                <img src="images/food__02.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/food__03.jpg" data-fancybox="gallery">
                                                <img src="images/food__03.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/food__01.jpg" data-fancybox="gallery">
                                                <img src="images/food__01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/food__02.jpg" data-fancybox="gallery">
                                                <img src="images/food__02.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a class="tour_gallery__item" href="images/food__03.jpg" data-fancybox="gallery">
                                                <img src="images/food__03.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>

                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="gallery_button_next"></div>
                                    <div class="gallery_button_prev"></div>
                                </div>
                            </div>

                            <h3 class="mb_40">Последний отзыв по этому туру</h3>
                            <div class="top_review mb_60">
                                <div class="top_review__heading">
                                    <div class="top_review__heading_star">
                                        <div class="raty" data-score="5" data-readOnly="true"></div>
                                    </div>
                                    <div class="top_review__heading_name">Инна</div>
                                    <div class="top_review__heading_date">4 октября 2018</div>
                                </div>
                                <div class="top_review__text">
                                    Супер гид! Карина провела просто великолепную экскурсию для нашей cемьи. Дети остались под большим впечатлением и мы глубоко признательны Карине за лучшую экскурсию в нашей жизни
                                </div>
                            </div>

                            <div class="review mb_30">
                                <div class="review__heading">
                                    <div class="review__heading_name">Инна</div>
                                    <div class="review__heading_date">4 октября 2018</div>
                                </div>
                                <div class="review__rating">
                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                </div>
                                <div class="review__text">
                                    Супер гид! Карина провела просто великолепную экскурсию для нашей cемьи. Дети остались под большим впечатлением и мы глубоко признательны Карине за лучшую экскурсию в нашей жизни
                                </div>
                            </div>

                            <div class="review mb_30">
                                <div class="review__heading">
                                    <div class="review__heading_name">Инна</div>
                                    <div class="review__heading_date">4 октября 2018</div>
                                </div>
                                <div class="review__rating">
                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                </div>
                                <div class="review__text">
                                    Супер гид! Карина провела просто великолепную экскурсию для нашей cемьи. Дети остались под большим впечатлением и мы глубоко признательны Карине за лучшую экскурсию в нашей жизни
                                </div>
                            </div>

                            <div class="review mb_50">
                                <div class="review__heading">
                                    <div class="review__heading_name">Инна</div>
                                    <div class="review__heading_date">4 октября 2018</div>
                                </div>
                                <div class="review__rating">
                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                </div>
                                <div class="review__text">
                                    Супер гид! Карина провела просто великолепную экскурсию для нашей cемьи. Дети остались под большим впечатлением и мы глубоко признательны Карине за лучшую экскурсию в нашей жизни
                                </div>
                            </div>


                            <h3>ОСТАВИТЬ ОТЗЫВ (только для тех кто участвовал)</h3>
                            <div class="new_review mb_30">
                                <div class="form_group">
                                    <div class="raty" data-score="" data-readOnly=""></div>
                                </div>
                                <div class="form_group">
                                    <textarea class="form_control form_control_radius" name="review" placeholder="" rows="6">Напишите Ваш отзыв об экскурсии</textarea>
                                </div>
                            </div>

                            <div class="pagination">
                                <a href="#" class="pagination__prev"></a>
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><span>...</span></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">8</a></li>
                                </ul>
                                <a href="#" class="pagination__next"></a>
                            </div>

                        </div>

                        <div class="main_right">
                            <div class="side">
                                <h6>Групповая экскурсия</h6>
                                <ul class="side__list mb_20">
                                    <li>Длительность: <strong>2 часа</strong></li>
                                    <li>Размер группы: <strong>До 6 человек</strong></li>
                                    <li>Дети:  <strong>Можно с детьми</strong></li>
                                    <li>Рейтинг:  <strong>5,0 (13 отзывов)</strong></li>
                                </ul>
                                <h6>дополнительные услуги</h6>
                                <ul class="side__list mb_20">
                                    <li>
                                        <label class="form_checkbox">
                                            <input type="checkbox" name="name" value="">
                                            <span>~50$ - Услуги переводчика</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="form_checkbox">
                                            <input type="checkbox" name="name" value="">
                                            <span>~50$ - Трансфер</span>
                                        </label>
                                    </li>
                                </ul>
                                <h6>Мой заказ ~ $150</h6>
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="">3 взрослых</option>
                                        <option value="">1 взрослых</option>
                                        <option value="">1 взрослый</option>
                                    </select>
                                </div>
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="">без детей</option>
                                        <option value="">1 ребенок</option>
                                        <option value="">2 ребенка</option>
                                        <option value="">3 ребенка</option>
                                    </select>
                                </div>
                                <a class="btn btn_long" href="#">Написать гиду бесплатно</a>
                            </div>
                        </div>

                    </div>

                    <h3>Еще туры этого гида</h3>

                    <div class="goods__row">

                        <div class="goods__elem">
                            <div class="goods">
                                <div class="goods__image">
                                    <a href="#" class="goods__image_wrap">
                                        <img src="images/goods__01.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="goods__tags">
                                        <a href="#">РОССИЯ</a><i></i><a href="#">Москва</a>
                                    </div>
                                    <div class="goods__sale goods__sale_red">
                                        <span>Скидка</span>
                                        <strong>10%</strong>
                                    </div>
                                    <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                    <div class="goods__rating">
                                        <div class="goods__rating_star">
                                            <div class="raty" data-score="5" data-readOnly="true"></div>
                                        </div>
                                        <span class="goods__rating_text">59 отзывов</span>
                                    </div>
                                </div>
                                <a href="#" class="goods__content">
                                    <div class="goods__name">Главные сокровища Лувра за 2 часа</div>
                                    <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                    <div class="goods__guide_wrap">
                                        <div class="goods__guide">
                                            <div class="goods__guide__photo">
                                                <img src="images/user__xs.png" alt="" class="img-fluid">
                                            </div>
                                            <span>Ольга</span> | <span>RU</span> | <span>EN</span>
                                        </div>
                                    </div>
                                    <div class="goods__price"><span>$58</span> за человека</div>
                                </a>
                            </div>
                        </div>

                        <div class="goods__elem">
                            <div class="goods">
                                <div class="goods__image">
                                    <a href="#" class="goods__image_wrap">
                                        <img src="images/goods__02.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="goods__tags">
                                        <a href="#">Венесуэла</a><i></i><a href="#">каракас</a>
                                    </div>
                                    <div class="goods__sale goods__sale_purple">
                                        <span>Скидка</span>
                                        <strong>15%</strong>
                                    </div>
                                    <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                    <div class="goods__rating">
                                        <div class="goods__rating_star">
                                            <div class="raty" data-score="4" data-readOnly="false"></div>
                                        </div>
                                        <span class="goods__rating_text">59 отзывов</span>
                                    </div>
                                </div>
                                <a href="#" class="goods__content">
                                    <div class="goods__name">ОБЗОРНАЯ ЭКСКУРСИЯ О. МАРГАРИТА.</div>
                                    <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                    <div class="goods__guide_wrap">
                                        <div class="goods__guide">
                                            <div class="goods__guide__photo">
                                                <img src="images/user__xs.png" alt="" class="img-fluid">
                                            </div>
                                            <span>Ольга</span> | <span>RU</span> | <span>EN</span>
                                        </div>
                                    </div>
                                    <div class="goods__price"><span>$350</span> за группу 1-4 человека</div>
                                </a>
                            </div>
                        </div>

                        <div class="goods__elem">
                            <div class="goods">
                                <div class="goods__image">
                                    <a href="#" class="goods__image_wrap">
                                        <img src="images/goods__03.jpg" class="img-fluid" alt="">
                                        <div class="goods__new"><span>НОВИНКА</span></div>
                                    </a>
                                    <div class="goods__tags">
                                        <a href="#">Австрия</a><i></i><a href="#">вена</a>
                                    </div>
                                    <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                    <div class="goods__rating">
                                        <div class="goods__rating_star">
                                            <div class="raty" data-score="4.5" data-readOnly="true"></div>
                                        </div>
                                        <span class="goods__rating_text">59 отзывов</span>
                                    </div>
                                </div>
                                <a href="#" class="goods__content">
                                    <div class="goods__name">вена</div>
                                    <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                    <div class="goods__guide_wrap">
                                        <div class="goods__guide">
                                            <div class="goods__guide__photo">
                                                <img src="images/user__xs.png" alt="" class="img-fluid">
                                            </div>
                                            <span>александра пушкина</span> | <span>RU</span> | <span>EN</span>
                                        </div>
                                    </div>
                                    <div class="goods__price"><span>$58</span> за человека</div>
                                </a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
