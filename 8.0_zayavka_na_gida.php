<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/guide_info__bg.jpg');">
            <div class="container">
                <h1>
                    <span>Заявка</span>
                    <br/>
                    <span>на гида</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="row">
                <div class="col col-xs-12 col-md-8 col-xl-9 col-gutter-lr">
                    <h3>Внесите основную информацию</h3>

                    <form class="form">

                        <div class="row">
                            <div class="col col-xs-10 col-sm-6 col-md-6 col-lg-5 col-xl-4 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Введите Ваше имя и фамилию</label>
                                    <input type="text" class="form_control" name="name" placeholder="" value="">
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Введите ваш номер мобильного телефона</label>
                                    <input type="text" class="form_control" name="phone" placeholder="" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_group mb_40">
                            <label class="form_label">Загрузите Ваши сертификаты</label>
                            <div class="form_gallery">
                                <div class="form_gallery__row">
                                    <div class="form_gallery__item">
                                        <div class="empty"></div>
                                    </div>
                                </div>
                                <label class="form_upload">
                                    <input type="file" name="file">
                                    <span>загрузить сертификаты</span>
                                </label>
                            </div>
                        </div>

                        <h3>Я в соц.сетях и других сервисах (только для администрации)</h3>
                        <div class="form_group mb_40">
                            <label class="form_label">Укажите адреса ваших соц.сетей</label>
                            <textarea class="form_control" name="social" placeholder="" rows="5"></textarea>
                        </div>

                        <h3>обо мне (доступно всем)</h3>

                        <label class="form_label">Города в которых я работаю</label>
                        <div class="row">
                            <div class="col col-xs-12 col-md-5 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="form_group mb_10">
                                    <input class="form_control" type="text" name="country" placeholder="" value="Беларусь">
                                </div>
                            </div>
                            <div class="col col-xs-9 col-md-5 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="form_group mb_10">
                                    <input class="form_control" type="text" name="city" placeholder="" value="Минск">
                                </div>
                            </div>
                            <div class="col col-xs-3 col-md-2 col-lg-2 col-xl-2 col-gutter-lr mb_10">
                                <button class="btn_change"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-xs-12 col-md-5 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="form_group mb_10">
                                    <input class="form_control" type="text" name="country" placeholder="" value="Россия">
                                </div>
                            </div>
                            <div class="col col-xs-9 col-md-5 col-lg-4 col-xl-3 col-gutter-lr">
                                <div class="form_group mb_10">
                                    <input class="form_control" type="text" name="city" placeholder="" value="Москва">
                                </div>
                            </div>
                            <div class="col col-xs-3 col-md-2 col-lg-2 col-xl-2 col-gutter-lr  mb_10">
                                <button class="btn_change"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="form_group pt_10 mb_40">
                            <label class="form_label">Информация о себе (достпно всем)</label>
                            <textarea class="form_control" name="social" placeholder="" rows="5"></textarea>
                        </div>

                        <h3>Мои услуги</h3>

                        <label class="form_label">Укажите стоимость услуг. Если какую-то услугу не оказываете оставьте поле пустым.</label>
                        <ul class="form_service">
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Услуги переводчика</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Трансфер</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Экскурсия по городу</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Организация мероприятий</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$,  Экскурсия по городу</span></div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Услуги переводчика</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Трансфер</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Экскурсия по городу</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Организация мероприятий</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$,  Экскурсия по городу</span></div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Услуги переводчика</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Трансфер</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Экскурсия по городу</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$, Организация мероприятий</span></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_group">
                                    <div class="form_price">
                                        <div class="form_price__input">
                                            <input class="form_control" type="text" name="price" placeholder="" value="">
                                        </div>
                                        <div class="form_price__legend"><span>$,  Экскурсия по городу</span></div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <div class="form_footer mb_30">
                            <div class="form_group mb_30">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="" value="">
                                    <span>С условиями <a href="#">использования сайта согласен(а)</a></span>
                                </label>
                            </div>
                            <button type="submit" class="btn">ОТПРАВИТЬ ЗАПРОС НА ДОСТУП К СЕРВИСУ КАК ГИД</button>
                        </div>

                    </form>

                </div>
                <div class="col col-xs-12 col-md-4 col-xl-3 col-gutter-lr">
                    <div class="info_box">
                        <h3 class="mb_10">ИНФО</h3>
                        <p>Каждый гид на сервисе проходит тщательную проверку на предмет профессиональной пригодности.</p>
                        <p>Также мы серьезно относимся к безопасности наших клиентов и просим подтвердить Ваш настоящий мобильный номер телефона</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
