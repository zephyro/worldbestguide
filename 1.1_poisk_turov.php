<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="page_heading_wrap">

                <div class="page_heading page_heading_one" style="background-image: url('img/search_top_bg.jpg');">
                    <div class="container">
                        <h1 contenteditable="true">
                            <span class="string_top_md">пОИСК ЛУЧШИХ</span><br/>
                            <span class="string_bottom_sm">ЭКСКУРСИонных туров</span>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="search_bar">
                <div class="container">
                    <div class="search_bar__wrap">
                        <div class="search_bar__container">
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Поиск туров">Поиск туров</option>
                                    <option value="Поиск туров">Поиск гидов</option>
                                    <option value="Поиск туров">Поиск статей</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Абхазия (34)">Абхазия (34)</option>
                                    <option value="Австрия (34)">Австрия (34)</option>
                                    <option value="Андорра (34)">Андорра (34)</option>
                                    <option value="Беларусь (34)">Беларусь (34)</option>
                                    <option value="Бельгия (34)">Бельгия (34)</option>
                                    <option value="Болгария (34)">Болгария  (34)</option>
                                    <option value="Великобритания  (314)">Великобритания  (314)</option>
                                    <option value="Венгрия  (34)">Венгрия  (34)</option>
                                    <option value="Германия  (34)">Германия  (34)</option>
                                    <option value="Гибралтар  (34)">Гибралтар  (34)</option>
                                    <option value="Греция  (34)">Греция  (34)</option>
                                    <option value="Дания  (34)">Дания  (34)</option>
                                    <option value="Ирландия  (34)">Ирландия  (34)</option>
                                    <option value="Испания  (34)">Испания  (34)</option>
                                    <option value="Италия  (34)">Италия  (34)</option>
                                    <option value="Латвия  (34)">Латвия  (34)</option>
                                    <option value="Литва  (34)">Литва  (34)</option>
                                    <option value="Люксембург  (34">Люксембург  (34)</option>
                                    <option value="Македония  (34)">Македония  (34)</option>
                                    <option value="Монако  (34)">Монако  (34)</option>
                                    <option value="Нидерланды  (34)">Нидерланды  (34)</option>
                                    <option value="Польша  (34)">Польша  (34)</option>
                                    <option value="Португалия  (34)">Португалия  (34)</option>
                                    <option value="Россия (34)">Россия (34)</option>
                                    <option value="Румыния (34)">Румыния (34)</option>
                                    <option value="Сан-Марино (34)">Сан-Марино (34)</option>
                                    <option value="Сербия (34)">Сербия (34)</option>
                                    <option value="Словакия (34)">Словакия (34)</option>
                                    <option value="Словения (34)">Словения (34)</option>
                                    <option value="Турция (34)">Турция (34)</option>
                                    <option value="Украина (34)">Украина (34)</option>
                                    <option value="Финляндия (34)">Финляндия (34)</option>
                                    <option value="Франция (34)">Франция (34)</option>
                                    <option value="Хорватия (34)">Хорватия (34)</option>
                                    <option value="Черногория (34)">Черногория (34)</option>
                                    <option value="Чехия (34)">Чехия (34)</option>
                                    <option value="Швейцария (34)">Швейцария (34)</option>
                                    <option value="Швеция (34)">Швеция (34)</option>
                                    <option value="Эстония (34)">Эстония (34)</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Все города">Все города</option>
                                    <option value="сЕУЛ  (34)">сЕУЛ  (34)</option>
                                    <option value="пАРИЖ  (34)">пАРИЖ  (34)</option>
                                    <option value="лОНДОН  (34)">лОНДОН  (34)</option>
                                    <option value="мОСКВА  (34)">мОСКВА  (34)</option>
                                    <option value="мИНСК  (34)">мИНСК  (34)</option>
                                    <option value="лЬВОВ  (34)">лЬВОВ  (34)</option>
                                    <option value="сАНКТ ПЕТЕРБУРГ  (34)">сАНКТ ПЕТЕРБУРГ  (34)</option>
                                    <option value="СОЧИ  (34)">СОЧИ  (34)</option>
                                    <option value="гРОДНО  (34)">гРОДНО  (34)</option>
                                    <option value="БРЕСТ  (34)">БРЕСТ  (34)</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="3 человека">3 человека</option>
                                    <option value="2 человека">2 человека</option>
                                    <option value="1 человек">1 человек</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Любая цена">Любая цена</option>
                                    <option value="до 50 тыс.">до 50 тыс.</option>
                                    <option value="от 50 до 150 тыс.">от 50 до 150 тыс.</option>
                                    <option value="от 150 тыс.">от 150 тыс.</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">
                    <div class="main_row">

                        <div class="main_sidebar">

                            <div class="sidebar_group">
                                <ul class="tours_nav">
                                    <li>
                                        <a href="#">
                                            <span>Все виды туров</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Водный тур, Рафтинг</span>
                                            <i class="tours_nav_icon_01">
                                                <svg class="ico-svg" viewBox="0 0 21 22" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_01" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Дайвинг</span>
                                            <i class="tours_nav_icon_02">
                                                <svg class="ico-svg" viewBox="0 0 22 23" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_02" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Развлекательный</span>
                                            <i class="tours_nav_icon_03">
                                                <svg class="ico-svg" viewBox="0 0 18 19" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_03" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>винный</span>
                                            <i class="tours_nav_icon_04">
                                                <svg class="ico-svg" viewBox="0 0 20 17" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_04" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Джиппинг</span>
                                            <i class="tours_nav_icon_05">
                                                <svg class="ico-svg" viewBox="0 0 26 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_05" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Железнодорожный</span>
                                            <i class="tours_nav_icon_06">
                                                <svg class="ico-svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>гастрономический ТУР</span>
                                            <i class="tours_nav_icon_07">
                                                <svg class="ico-svg" viewBox="0 0 21 20" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Бизнес тур</span>
                                            <i class="tours_nav_icon_08">
                                                <svg class="ico-svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_08" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Temple stay</span>
                                            <i class="tours_nav_icon_09">
                                                <svg class="ico-svg" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_09" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>лечение за рубежом</span>
                                            <i class="tours_nav_icon_10">
                                                <svg class="ico-svg" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_10" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Исторические, Обзорные</span>
                                            <i class="tours_nav_icon_11">
                                                <svg class="ico-svg" viewBox="0 0 17 21" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_11" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Охота и рыбалка</span>
                                            <i class="tours_nav_icon_12">
                                                <svg class="ico-svg" viewBox="0 0 29 23" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_12" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Пешеходная экскурсия</span>
                                            <i class="tours_nav_icon_13">
                                                <svg class="ico-svg" viewBox="0 0 11 27" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_13" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Приключения и экстрим</span>
                                            <i class="tours_nav_icon_14">
                                                <svg class="ico-svg" viewBox="0 0 19 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_14" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Сафари и треккинг</span>
                                            <i class="tours_nav_icon_15">
                                                <svg class="ico-svg" viewBox="0 0 20 16" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_15" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Шопинг тур</span>
                                            <i class="tours_nav_icon_16">
                                                <svg class="ico-svg" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_16" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Экологический тур</span>
                                            <i class="tours_nav_icon_17">
                                                <svg class="ico-svg" viewBox="0 0 20 22" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_17" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Экспедиция</span>
                                            <i class="tours_nav_icon_18">
                                                <svg class="ico-svg" viewBox="0 0 23 14" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tours_18" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="main_content">
                            
                            <div class="goods__row">

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/goods__01.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a><i></i><a href="#">Москва</a>
                                            </div>
                                            <div class="goods__sale goods__sale_red">
                                                <span>Скидка</span>
                                                <strong>10%</strong>
                                            </div>
                                            <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Главные сокровища Лувра за 2 часа</div>
                                            <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                            <div class="goods__guide_wrap">
                                                <div class="goods__guide">
                                                    <div class="goods__guide__photo">
                                                        <img src="images/user__xs.png" alt="" class="img-fluid">
                                                    </div>
                                                    <span>Ольга</span> | <span>RU</span> | <span>EN</span>
                                                </div>
                                            </div>
                                            <div class="goods__price"><span>$58</span> за человека</div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/goods__02.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">Венесуэла</a><i></i><a href="#">каракас</a>
                                            </div>
                                            <div class="goods__sale goods__sale_purple">
                                                <span>Скидка</span>
                                                <strong>15%</strong>
                                            </div>
                                            <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="4" data-readOnly="false"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">ОБЗОРНАЯ ЭКСКУРСИЯ О. МАРГАРИТА.</div>
                                            <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                            <div class="goods__guide_wrap">
                                                <div class="goods__guide">
                                                    <div class="goods__guide__photo">
                                                        <img src="images/user__xs.png" alt="" class="img-fluid">
                                                    </div>
                                                    <span>Ольга</span> | <span>RU</span> | <span>EN</span>
                                                </div>
                                            </div>
                                            <div class="goods__price"><span>$350</span> за группу 1-4 человека</div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/goods__03.jpg" class="img-fluid" alt="">
                                                <div class="goods__new"><span>НОВИНКА</span></div>
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">Австрия</a><i></i><a href="#">вена</a>
                                            </div>
                                            <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="4.5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">вена</div>
                                            <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                            <div class="goods__guide_wrap">
                                                <div class="goods__guide">
                                                    <div class="goods__guide__photo">
                                                        <img src="images/user__xs.png" alt="" class="img-fluid">
                                                    </div>
                                                    <span>александра пушкина</span> | <span>RU</span> | <span>EN</span>
                                                </div>
                                            </div>
                                            <div class="goods__price"><span>$58</span> за человека</div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/goods__01.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a><i></i><a href="#">Москва</a>
                                            </div>
                                            <div class="goods__sale goods__sale_red">
                                                <span>Скидка</span>
                                                <strong>10%</strong>
                                            </div>
                                            <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Главные сокровища Лувра за 2 часа</div>
                                            <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                            <div class="goods__guide_wrap">
                                                <div class="goods__guide">
                                                    <div class="goods__guide__photo">
                                                        <img src="images/user__xs.png" alt="" class="img-fluid">
                                                    </div>
                                                    <span>Ольга</span> | <span>RU</span> | <span>EN</span>
                                                </div>
                                            </div>
                                            <div class="goods__price"><span>$58</span> за человека</div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/goods__02.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">Венесуэла</a><i></i><a href="#">каракас</a>
                                            </div>
                                            <div class="goods__sale goods__sale_purple">
                                                <span>Скидка</span>
                                                <strong>15%</strong>
                                            </div>
                                            <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="4" data-readOnly="false"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">ОБЗОРНАЯ ЭКСКУРСИЯ О. МАРГАРИТА.</div>
                                            <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                            <div class="goods__guide_wrap">
                                                <div class="goods__guide">
                                                    <div class="goods__guide__photo">
                                                        <img src="images/user__xs.png" alt="" class="img-fluid">
                                                    </div>
                                                    <span>Ольга</span> | <span>RU</span> | <span>EN</span>
                                                </div>
                                            </div>
                                            <div class="goods__price"><span>$350</span> за группу 1-4 человека</div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/goods__03.jpg" class="img-fluid" alt="">
                                                <div class="goods__new"><span>НОВИНКА</span></div>
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">Австрия</a><i></i><a href="#">вена</a>
                                            </div>
                                            <div class="goods__time"><span class="goods__time_icon" data-toltip="обзорный тур"></span>2 часа</div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="4.5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">вена</div>
                                            <div class="goods__text">Насыщенное экспресс-знакомство с шедеврами музея без ущерба для впечатлений. Интересное путешествие по стране ос. Вы получите нереальные впечатление о жизни для взро...</div>
                                            <div class="goods__guide_wrap">
                                                <div class="goods__guide">
                                                    <div class="goods__guide__photo">
                                                        <img src="images/user__xs.png" alt="" class="img-fluid">
                                                    </div>
                                                    <span>александра пушкина</span> | <span>RU</span> | <span>EN</span>
                                                </div>
                                            </div>
                                            <div class="goods__price"><span>$58</span> за человека</div>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="goods_view">
                                <a href="#" class="btn_xl btn_long">пОКАЗАТЬ ЕЩЕ</a>
                            </div>
                            <div class="pagination">
                                <a href="#" class="pagination__prev"></a>
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><span>...</span></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">8</a></li>
                                </ul>
                                <a href="#" class="pagination__next"></a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
