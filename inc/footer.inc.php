<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__info">

                <div class="footer__top">
                    <a class="footer__logo" href="/">
                        <img src="img/footer__logo.svg" class="img-fluid" alt="">
                    </a>
                    <div class="footer__nav">
                        <ul>
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Партнерам</a></li>
                            <li><a href="#">Условия</a></li>
                            <li><a href="#">Обратная связь</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Регистрация гида</a></li>
                            <li><a href="#">Страница гида</a></li>
                            <li><a href="#">Страница туриста</a></li>
                            <li><a href="#">Информация для гидов</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer__social">
                    <div class="footer__social_title">Мы в соц.сетях!</div>
                    <ul>
                        <li>
                            <a href="#">
                                <div class="footer__social_icon">
                                    <img src="img/icon__telegram_plane.svg" alt="" class="img-fluid">
                                </div>
                                <span>telegram: @WorldBesTravel.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="footer__social_icon">
                                    <img src="img/icon__facebook_f.svg" alt="" class="img-fluid">
                                </div>
                                <span>facebook.com/WorldBesTravel.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="footer__social_icon">
                                    <img src="img/icon__twitter.svg" alt="" class="img-fluid">
                                </div>
                                <span>twitter.com/WorldBesTravel.com</span>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="footer__travel">
                <div class="footer__travel_title">экскурсионные туры в</div>
                <ul class="footer__travel_country">
                    <li><a href="#">Австралия</a></li>
                    <li><a href="#">Австрия</a></li>
                    <li><a href="#">Азербайджан</a></li>
                    <li><a href="#">Армения</a></li>
                    <li><a href="#">Беларусь</a></li>
                    <li><a href="#">Бельгия</a></li>
                    <li><a href="#">Болгария</a></li>
                    <li><a href="#">Бразилия</a></li>
                    <li><a href="#">Великобритания</a></li>
                    <li><a href="#">Венгрия</a></li>
                    <li><a href="#">Венесуэла</a></li>
                    <li><a href="#">Вьетнам</a></li>

                    <li><a href="#">Германия</a></li>
                    <li><a href="#">Гонконг</a></li>
                    <li><a href="#">Греция</a></li>
                    <li><a href="#">Грузия</a></li>
                    <li><a href="#">Дания</a></li>
                    <li><a href="#">Египет</a></li>
                    <li><a href="#">Израиль</a></li>
                    <li><a href="#">Индия</a></li>
                    <li><a href="#">Индонезия</a></li>
                    <li><a href="#">Иордания</a></li>
                    <li><a href="#">Ирландия</a></li>
                    <li><a href="#">Испания</a></li>

                    <li><a href="#">Италия</a></li>
                    <li><a href="#">Казахстан</a></li>
                    <li><a href="#">Камбоджа</a></li>
                    <li><a href="#">Канада</a></li>
                    <li><a href="#">Кипр</a></li>
                    <li><a href="#">Киргизия</a></li>
                    <li><a href="#">Китай</a></li>
                    <li><a href="#">Коста-Рика</a></li>
                    <li><a href="#">Куба</a></li>
                    <li><a href="#">Лаос</a></li>
                    <li><a href="#">Латвия</a></li>
                    <li><a href="#">Ливан</a></li>

                    <li><a href="#">Литва</a></li>
                    <li><a href="#">Мадагаскар</a></li>
                    <li><a href="#">Македония</a></li>
                    <li><a href="#">Малайзия</a></li>
                    <li><a href="#">Марокко</a></li>
                    <li><a href="#">Мексика</a></li>
                    <li><a href="#">Непал</a></li>
                    <li><a href="#">НидерландыОАЭ</a></li>
                    <li><a href="#">Оман</a></li>
                    <li><a href="#">Палестина</a></li>
                    <li><a href="#">Панама</a></li>

                    <li><a href="#">Перу</a></li>
                    <li><a href="#">Польша</a></li>
                    <li><a href="#">Португалия</a></li>
                    <li><a href="#">Россия</a></li>
                    <li><a href="#">Румыния</a></li>
                    <li><a href="#">США</a></li>
                    <li><a href="#">Сейшелы</a></li>
                    <li><a href="#">Сербия</a></li>
                    <li><a href="#">Словения</a></li>
                    <li><a href="#">Таиланд</a></li>
                    <li><a href="#">Тунис</a></li>
                    <li><a href="#">Турция</a></li>

                    <li><a href="#">Узбекистан</a></li>
                    <li><a href="#">Украина</a></li>
                    <li><a href="#">Филиппины</a></li>
                    <li><a href="#">Финляндия</a></li>
                    <li><a href="#">Франция</a></li>
                    <li><a href="#">Хорватия</a></li>
                    <li><a href="#">Черногория</a></li>
                    <li><a href="#">Чехия</a></li>
                    <li><a href="#">Чили</a></li>
                    <li><a href="#">Швейцария</a></li>
                    <li><a href="#">Швеция</a></li>
                    <li><a href="#">Шри-Ланка</a></li>

                    <li><a href="#">Эстония</a></li>
                    <li><a href="#">Эфиопия</a></li>
                    <li><a href="#">Южная Корея </a></li>
                    <li><a href="#">Япония</a></li>

                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="footer__bottom">
    <div class="container">
        <div class="footer__bottom_text">WorldBesGuide.com @2018 | Адрес </div>
        <div class="footer__bottom_payment">
            <span><img src="img/icon__visa.svg"></span>
            <span><img src="img/icon__mc.svg"></span>
            <span><img src="img/icon__mir.svg"></span>
        </div>
    </div>
</div>


<!-- Filter: https://css-tricks.com/gooey-effect/ -->
<svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
    <defs>
        <filter id="goo">
            <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
            <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
        </filter>
    </defs>
</svg>
