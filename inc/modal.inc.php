
<!-- Вход -->
<div class="hide">
    <a href="#enter" class="open_reg btn_modal"></a>
    <div class="modal" id="enter">
        <a href="#" class="modal__prev">
            <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
        </a>
        <div class="modal__main">
            <div class="modal__wrap">
                <div class="modal__heading">
                    <div class="modal__heading_title">Войти</div>
                    <span>или</span>
                    <a href="#">зарегистрироваться</a>
                </div>
                <div class="modal__body">
                    <form class="form">

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>Е-MAIL</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>ПАРОЛЬ</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form_group mb_25">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="" value="">
                                        <span>запомнить меня</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <button type="submit" class="btn_main btn_long btn_shadow">Войти</button>
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <div class="modal__text"><a href="#">восстановить пароль</a></div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- -->

<!-- Регистрация -->
<div class="hide">
    <a href="#reg" class="open_reg btn_modal"></a>
    <div class="modal" id="reg">
        <a href="#" class="modal__prev">
            <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
        </a>
        <div class="modal__main">
            <div class="modal__wrap">
                <div class="modal__heading">
                    <div class="modal__heading_title">зарегистрироваться</div>
                    <span>или</span>
                    <a href="#">Войти</a>
                </div>
                <div class="modal__body">
                    <form class="form">

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>Е-MAIL</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>ПАРОЛЬ</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form_group mb_25">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="" value="">
                                        <span>С условиями <a href="#">использования сайта согласен(а)</a></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <button type="submit" class="btn_main btn_long btn_shadow">регистрация</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- -->

<!-- Восстановление пароля -->
<div class="hide">
    <a href="#recovery" class="open_recovery btn_modal"></a>
    <div class="modal" id="recovery">
        <a href="#" class="modal__prev">
            <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
        </a>
        <div class="modal__main">
            <div class="modal__wrap">
                <div class="modal__heading">
                    <div class="modal__heading_title">Восстановление пароля</div>
                </div>
                <div class="modal__body">
                    <form class="form">

                        <div class="form_group mb_25">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>ВАШ ЕMAIL</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <button type="submit" class="btn_main btn_long btn_shadow">восстановить</button>
                                </div>
                            </div>
                        </div>
                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right text-center">
                                    <div class="modal__text"><span class="color_blue">или</span> <a href="#">зарегистрироваться</a></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- -->


<!-- Новый пароль -->
<div class="hide">
    <a href="#new_pass" class="open_new_pas btn_modal"></a>
    <div class="modal" id="new_pass">
        <a href="#" class="modal__prev">
            <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
        </a>
        <div class="modal__main">
            <div class="modal__wrap">
                <div class="modal__heading">
                    <div class="modal__heading_title">Введите Ваш новый пароль</div>
                </div>
                <div class="modal__body">
                    <form class="form">
                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>ПАРОЛЬ</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form_group mb_25">
                            <div class="modal__row">
                                <div class="modal__row_left">
                                    <label class="form_label"><span>повторите ПАРОЛЬ</span></label>
                                </div>
                                <div class="modal__row_right">
                                    <input type="text" class="form_control" name="" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form_group">
                            <div class="modal__row">
                                <div class="modal__row_left"></div>
                                <div class="modal__row_right">
                                    <button type="submit" class="btn_main btn_long btn_shadow">сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- -->