<header class="header">
    <div class="container">

        <div class="header__top">
            <ul class="header__topnav">
                <li><a href="#">Для гидов</a></li>
                <li><a href="#">О нас</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
            <ul class="header__lng">
                <li><a href="#" class="ru"><span>ru</span></a></li>
                <li><a href="#" class="en"><span>en</span></a></li>
            </ul>
        </div>

        <div class="header__main">
            <a class="header__logo" href="#">
                <img src="img/header__logo.svg" class="img-fluid" alt="">
            </a>

            <a class="header__toggle nav_toggle" href="#">
                <span></span>
                <span></span>
                <span></span>
            </a>

            <nav class="header__nav">

                <a class="header__travel" href="#">
                    <span>2</span>
                    <img src="img/header__icon.svg" class="img-fluid" alt="">
                </a>

                <ul class="header__menu">
                    <li class="active"><a href="#">Поиск туров</a></li>
                    <li><a href="#">Поиск гидов</a></li>
                    <li><a href="#">Блоги</a></li>
                </ul>

                <div class="header__auth">
                    <a href="#" class="auth_ok">МОЙ КАБИНЕТ</a>
                </div>

            </nav>
        </div>
    </div>
</header>