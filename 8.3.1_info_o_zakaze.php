<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/header__bg_3.jpg');">
            <div class="container">
                <h1>
                    <span>История заказа</span>
                    <br>
                    <span>№493859249</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <?php include('inc/user_sidebar.inc.php') ?>
                    <!-- -->

                </div>
                <div class="main_content">

                    <div class="chat">
                        <div class="chat__info">
                            <div class="chat__info_date">13:12, 4jan 2018</div>
                            <div class="chat__info_title">Новый тур</div>
                        </div>
                        <div class="chat__content">
                            <h4>ИНФОРМАЦИЯ О ЗАКАЗЕ</h4>
                            <ul class="chat__content_info">
                                <li>Статус: <strong class="color_green text-uppercase">АКТИВНО</strong> <strong>11.15.2019, 12:32</strong></li>
                                <li> Дата проведения тура: <strong>15.12.2019, 14:00</strong></li>
                                <li>Количество участников:  <strong>7 взрослых 2 детей</strong></li>
                            </ul>

                            <h4>ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ</h4>
                            <div class="chat__service">
                                <ul>
                                    <li>
                                        <i><img src="img/chat_service__icon_01.svg" alt=""></i>
                                        <span>Услуги переводчика</span>
                                    </li>
                                    <li>
                                        <i><img src="img/chat_service__icon_02.svg" alt=""></i>
                                        <span>Оформление визы</span>
                                    </li>
                                    <li>
                                        <i><img src="img/chat_service__icon_03.svg" alt=""></i>
                                        <span>Пешеходная экскурсия по городу</span>
                                    </li>
                                </ul>
                                <ul>
                                    <li>
                                        <i><img src="img/chat_service__icon_01.svg" alt=""></i>
                                        <span>Услуги переводчика</span>
                                    </li>
                                    <li>
                                        <i><img src="img/chat_service__icon_02.svg" alt=""></i>
                                        <span>Оформление визы</span>
                                    </li>
                                    <li>
                                        <i><img src="img/chat_service__icon_03.svg" alt=""></i>
                                        <span>Пешеходная экскурсия по городу</span>
                                    </li>
                                </ul>
                            </div>

                            <h4>КОММЕНТАРИЙ К ЗАКАЗУ</h4>
                            <div class="chat__content_text">
                                Хотелось бы встретиться после 12 часов, подскажете сможете ли встретить в это время?<br/>
                                И еще, сможете доставить в нужное место?
                            </div>

                        </div>
                    </div>

                    <div class="chat">
                        <div class="chat__info">
                            <div class="chat__info_date">гид - 13:12, 4jan 2018</div>
                            <div class="chat__info_title">Виктор третьяков</div>
                        </div>
                        <div class="chat__content">
                            <div class="chat__content_text">
                                Подскажите, а Вам нужен еще переводчик дополнительный или нет?<br/>
                                Или может фотограф?
                            </div>
                        </div>
                    </div>

                    <div class="chat">
                        <div class="chat__info chat__info_green">
                            <div class="chat__info_date">клиент - 13:12, 4jan 2018</div>
                            <div class="chat__info_title">Александр пушков</div>
                        </div>
                        <div class="chat__content chat__content_green">
                            <div class="chat__content_text">
                                Нет, спасибо! Все ок!<br/>
                                По приезду наберите мне
                            </div>
                        </div>
                    </div>

                    <div class="chat">
                        <div class="chat__info">
                            <div class="chat__info_date">гид - 13:12, 4jan 2018</div>
                            <div class="chat__info_title">Виктор третьяков</div>
                        </div>
                        <div class="chat__content">
                            <div class="chat__content_text">
                                Нет, спасибо! Все ок!<br/>
                                По приезду наберите мне
                            </div>
                        </div>
                    </div>

                    <div class="chat chat_last">
                        <div class="chat__info">
                            <div class="chat__info_title color_blue pt_10">добавить ответ</div>
                        </div>
                        <div class="chat__content">
                            <div class="chat__content_actions">
                                <a href="#">Показать контакты клиента</a>
                                <span>|</span>
                                <a href="#">Вернуть средства</a>
                            </div>
                            <div class="form_group">
                                <textarea class="form_control" name="message" placeholder="" rows="6"></textarea>
                            </div>
                            <div class="text-right">
                                <button class="btn" type="submit">ОТПРАВИТЬ</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->


</body>
</html>
