<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="page_heading_wrap">

                <div class="page_heading page_heading_one" style="background-image: url('img/search_guide_bg.jpg');">
                    <div class="container">
                        <h1>
                            <span>пОИСК ЛУЧШИХ</span>
                            <br/>
                            <span>гидов по всему миру</span>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="search_bar">
                <div class="container">
                    <div class="search_bar__wrap">
                        <div class="search_bar__container">
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Поиск туров">Поиск гидов</option>
                                    <option value="Поиск туров">Поиск туров</option>
                                    <option value="Поиск туров">Поиск статей</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Абхазия (34)">Абхазия (34)</option>
                                    <option value="Австрия (34)">Австрия (34)</option>
                                    <option value="Андорра (34)">Андорра (34)</option>
                                    <option value="Беларусь (34)">Беларусь (34)</option>
                                    <option value="Бельгия (34)">Бельгия (34)</option>
                                    <option value="Болгария (34)">Болгария  (34)</option>
                                    <option value="Великобритания  (314)">Великобритания  (314)</option>
                                    <option value="Венгрия  (34)">Венгрия  (34)</option>
                                    <option value="Германия  (34)">Германия  (34)</option>
                                    <option value="Гибралтар  (34)">Гибралтар  (34)</option>
                                    <option value="Греция  (34)">Греция  (34)</option>
                                    <option value="Дания  (34)">Дания  (34)</option>
                                    <option value="Ирландия  (34)">Ирландия  (34)</option>
                                    <option value="Испания  (34)">Испания  (34)</option>
                                    <option value="Италия  (34)">Италия  (34)</option>
                                    <option value="Латвия  (34)">Латвия  (34)</option>
                                    <option value="Литва  (34)">Литва  (34)</option>
                                    <option value="Люксембург  (34">Люксембург  (34)</option>
                                    <option value="Македония  (34)">Македония  (34)</option>
                                    <option value="Монако  (34)">Монако  (34)</option>
                                    <option value="Нидерланды  (34)">Нидерланды  (34)</option>
                                    <option value="Польша  (34)">Польша  (34)</option>
                                    <option value="Португалия  (34)">Португалия  (34)</option>
                                    <option value="Россия (34)">Россия (34)</option>
                                    <option value="Румыния (34)">Румыния (34)</option>
                                    <option value="Сан-Марино (34)">Сан-Марино (34)</option>
                                    <option value="Сербия (34)">Сербия (34)</option>
                                    <option value="Словакия (34)">Словакия (34)</option>
                                    <option value="Словения (34)">Словения (34)</option>
                                    <option value="Турция (34)">Турция (34)</option>
                                    <option value="Украина (34)">Украина (34)</option>
                                    <option value="Финляндия (34)">Финляндия (34)</option>
                                    <option value="Франция (34)">Франция (34)</option>
                                    <option value="Хорватия (34)">Хорватия (34)</option>
                                    <option value="Черногория (34)">Черногория (34)</option>
                                    <option value="Чехия (34)">Чехия (34)</option>
                                    <option value="Швейцария (34)">Швейцария (34)</option>
                                    <option value="Швеция (34)">Швеция (34)</option>
                                    <option value="Эстония (34)">Эстония (34)</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Все города">Все города</option>
                                    <option value="сЕУЛ  (34)">сЕУЛ  (34)</option>
                                    <option value="пАРИЖ  (34)">пАРИЖ  (34)</option>
                                    <option value="лОНДОН  (34)">лОНДОН  (34)</option>
                                    <option value="мОСКВА  (34)">мОСКВА  (34)</option>
                                    <option value="мИНСК  (34)">мИНСК  (34)</option>
                                    <option value="лЬВОВ  (34)">лЬВОВ  (34)</option>
                                    <option value="сАНКТ ПЕТЕРБУРГ  (34)">сАНКТ ПЕТЕРБУРГ  (34)</option>
                                    <option value="СОЧИ  (34)">СОЧИ  (34)</option>
                                    <option value="гРОДНО  (34)">гРОДНО  (34)</option>
                                    <option value="БРЕСТ  (34)">БРЕСТ  (34)</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="3 человека">3 человека</option>
                                    <option value="2 человека">2 человека</option>
                                    <option value="1 человек">1 человек</option>
                                </select>
                            </div>
                            <div class="search_bar__elem">
                                <select class="form_select form_select_style">
                                    <option value="Любая цена">Любая цена</option>
                                    <option value="до 50 тыс.">до 50 тыс.</option>
                                    <option value="от 50 до 150 тыс.">от 50 до 150 тыс.</option>
                                    <option value="от 150 тыс.">от 150 тыс.</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">
                    <div class="main_row">
                        <div class="main_sidebar">
                            <div class="sidebar_group">
                                <div class="sidebar_heading">Все виды туров</div>
                                <ul  class="service_nav">
                                    <li>
                                        <a href="#">
                                            <span>Услуги переводчика</span>
                                            <i class="service_nav_icon_01">
                                                <svg class="ico-svg" viewBox="0 0 23 24" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_01" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Трансфер</span>
                                            <i class="service_nav_icon_02">
                                                <svg class="ico-svg" viewBox="0 0 24 14" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_02" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Образование за рубежом</span>
                                            <i class="service_nav_icon_03">
                                                <svg class="ico-svg" viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_03" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Оформление визы</span>
                                            <i class="service_nav_icon_04">
                                                <svg class="ico-svg" viewBox="0 0 19 20" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_04" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Организация мероприятий</span>
                                            <i class="service_nav_icon_05">
                                                <svg class="ico-svg" viewBox="0 0 23 22" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_05" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Шопинг сопровождение</span>
                                            <i class="service_nav_icon_06">
                                                <svg class="ico-svg" viewBox="0 0 18 19" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Услуги фотографа</span>
                                            <i class="service_nav_icon_07">
                                                <svg class="ico-svg" viewBox="0 0 18 19" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Лечение за рубежом</span>
                                            <i class="service_nav_icon_08">
                                                <svg class="ico-svg" viewBox="0 0 19 17" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_08" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Организация свадеб</span>
                                            <i class="service_nav_icon_09">
                                                <svg class="ico-svg" viewBox="0 0 21 16" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__service_09" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sidebar_group">
                                <div class="sidebar_heading">Знание языков</div>
                                <ul class="sidebar_filter">
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Русский язык</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Английский язык</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Испанский</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Португальский</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Французский</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Корейский</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Немецкий</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Японский</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="sidebar_filter__elem">
                                            <input type="checkbox" name="checkbox" value="">
                                            <span>Китайский</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="sidebar_group">
                                <div class="sidebar_button">
                                    <a href="#" class="btn">стать гидом</a>
                                </div>
                            </div>
                        </div>
                        <div class="main_content">
                            <div class="goods__row">

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_01.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ЧЕХИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_03.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ВЕНЕСУЭЛЛА</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Александра с очень дли</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_01.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ЧЕХИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_03.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ВЕНЕСУЭЛЛА</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Александра с очень дли</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_01.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ЧЕХИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_03.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ВЕНЕСУЭЛЛА</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Александра с очень дли</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="goods_view">
                                <a href="#" class="btn_xl btn_long">пОКАЗАТЬ ЕЩЕ</a>
                            </div>
                            <div class="pagination">
                                <a href="#" class="pagination__prev"></a>
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><span>...</span></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">8</a></li>
                                </ul>
                                <a href="#" class="pagination__next"></a>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
