<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_home">

            <div class="page_wrap">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <section class="guide">
                    <div class="container">
                        <h1 class="guide__h1"><span>Лицензированные</span> гиды <br/>переводчики со всего мира</h1>

                        <div class="selection">
                            <form class="form">

                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="selection_group">
                                            <div class="selection_switch">
                                                <label>
                                                    <input type="radio" name="selection_type" value="1" checked>
                                                    <span>Я ищу тур</span>
                                                </label>
                                                <label>
                                                    <input type="radio" name="selection_type" value="2">
                                                    <span>Я ищу гида</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="selection_group">
                                            <select class="selection_select">
                                                <option value="Любой тур">Любой тур</option>
                                                <option value="Тур 1">Тур 1</option>
                                                <option value="Тур 2">Тур 2</option>
                                                <option value="Тур 3">Тур 3</option>
                                                <option value="Тур 4">Тур 4</option>
                                                <option value="Тур 5">Тур 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="selection_group">
                                    <input type="text" class="selection_input" name="" placeholder="" autofocus>
                                </div>

                                <div class="selection_block">
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Нью-йорк">
                                        <span>Нью-йорк</span>
                                    </label>
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Москва">
                                        <span>Москва</span>
                                    </label>
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Сеул">
                                        <span>Сеул</span>
                                    </label>
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Париж">
                                        <span>Париж</span>
                                    </label>
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Лондон">
                                        <span>Лондон</span>
                                    </label>
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Пекин">
                                        <span>Пекин</span>
                                    </label>
                                    <label class="selection_radio">
                                        <input type="radio" name="place" value="Рим">
                                        <span>Рим</span>
                                    </label>
                                </div>

                                <button type="submit" class="btn_large">Показать все туры</button>

                            </form>
                        </div>

                        <ul class="guide__advantage">
                            <li>Только лучшие<br/>гиды</li>
                            <li>100%<br/>бесплатно</li>
                            <li>Только с<br/>лицензией</li>
                        </ul>

                        <div class="guide__partners">
                            <a href="#">
                                <img src="images/logo__booking.png" class="img-fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="images/logo__skyscaner.png" class="img-fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="images/logo__airbnb.png" class="img-fluid" alt="">
                            </a>
                        </div>

                        <div class="guide__feature">
                            <ul>
                                <li>
                                    <strong>750</strong>
                                    <span>гидов</span>
                                </li>
                                <li>
                                    <strong>120</strong>
                                    <span>стран</span>
                                </li>
                                <li>
                                    <strong>1050</strong>
                                    <span>отзывов</span>
                                </li>
                            </ul>
                        </div>

                    </div>
                </section>

                <section class="staff">
                    <div class="container">

                        <div class="staff__heading">
                            <div class="staff__heading_title">Топ<br/>гидов</div>
                            <div class="staff__heading_arrow staff__heading_comment"><a href="#">читать отзывыо нас</a></div>
                            <div class="staff__heading_arrow staff__heading_stories"><a href="#">читать истории от наших гидов</a></div>
                        </div>

                        <div class="staff__slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="#review01" class="staff__user">
                                        <div class="staff__user_place">Сеул</div>
                                        <div class="staff__user_image active">
                                            <div class="staff__user_wrap">
                                                <img src="images/staff__01.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#review02" class="staff__user">
                                        <div class="staff__user_place">Сеул</div>
                                        <div class="staff__user_image">
                                            <div class="staff__user_wrap">
                                                <img src="images/staff__02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#review03" class="staff__user">
                                        <div class="staff__user_place">Нью-Йорк</div>
                                        <div class="staff__user_image">
                                            <div class="staff__user_wrap">
                                                <img src="images/staff__03.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#review04" class="staff__user">
                                        <div class="staff__user_place">Москва</div>
                                        <div class="staff__user_image">
                                            <div class="staff__user_wrap">
                                                <img src="images/staff__04.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#review05" class="staff__user">
                                        <div class="staff__user_place">Мехико</div>
                                        <div class="staff__user_image">
                                            <div class="staff__user_wrap">
                                                <img src="images/staff__05.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="staff__info">

                            <div class="staff__item active" id="review01">
                                <div class="staff__name">Карина Дубовицкая</div>
                                <div class="staff__text">
                                    <div class="staff__text_wrap">
                                        Супер гид! Карина провела просто великолепную экскурсию для нашей семьи.
                                        Дети остались под большим впечатлением и мы глубоко признательны Карине
                                        за лучшую экскурсию в нашей жизни
                                    </div>
                                </div>
                            </div>
                            <div class="staff__item" id="review02">
                                <div class="staff__name">Елена Смирнова</div>
                                <div class="staff__text">
                                    <div class="staff__text_wrap">
                                        Супер гид! Карина провела просто великолепную экскурсию для нашей семьи.
                                        Дети остались под большим впечатлением и мы глубоко признательны Карине
                                        за лучшую экскурсию в нашей жизни
                                    </div>
                                </div>
                            </div>
                            <div class="staff__item" id="review03">
                                <div class="staff__name">Ольга Петрова</div>
                                <div class="staff__text">
                                    <div class="staff__text_wrap">
                                        Супер гид! Карина провела просто великолепную экскурсию для нашей семьи.
                                        Дети остались под большим впечатлением и мы глубоко признательны Карине
                                        за лучшую экскурсию в нашей жизни
                                    </div>
                                </div>
                            </div>
                            <div class="staff__item" id="review04">
                                <div class="staff__name">Сергей Павлов</div>
                                <div class="staff__text">
                                    <div class="staff__text_wrap">
                                        Супер гид! Карина провела просто великолепную экскурсию для нашей семьи.
                                        Дети остались под большим впечатлением и мы глубоко признательны Карине
                                        за лучшую экскурсию в нашей жизни
                                    </div>
                                </div>
                            </div>
                            <div class="staff__item" id="review05">
                                <div class="staff__name">Андрей Смолов</div>
                                <div class="staff__text">
                                    <div class="staff__text_wrap">
                                        Супер гид! Карина провела просто великолепную экскурсию для нашей семьи.
                                        Дети остались под большим впечатлением и мы глубоко признательны Карине
                                        за лучшую экскурсию в нашей жизни
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="service_block">
                    <div class="container">
                        <div class="heading">Услуги, наших гидов</div>
                        <ul class="service_list">
                            <li>
                                <a href="#"><span>Услуги<br/>переводчика</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Трансфер</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Лечение за<br/>рубежом</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Оформление<br/>визы</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Организация<br/>мероприятий</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Шопинг - <br/>сопровождение</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Услуги фотогрофа,<br/>фотосъемка</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Образование <br/>за рубежом</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Организация <br/>свадеб</span></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <section class="popular">
                    <div class="container">
                        <div class="heading">популярные <br/>города</div>
                        <div class="popular__slider swiper-container">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__01.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Лондон</span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__02.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Париж</span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__03.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Сеул</span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__04.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Москва</span>
                                    </a>
                                </div>

                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__01.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Лондон</span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__02.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Париж</span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__03.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Сеул</span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="popular__city">
                                        <img src="images/city__04.jpg" class="img-fluid" alt="">
                                        <span class="popular__city_name">Москва</span>
                                    </a>
                                </div>

                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Navigation -->
                            <div class="slider_button slider_button_next"></div>
                            <div class="slider_button slider_button_prev"></div>
                        </div>
                    </div>
                </section>

                <section class="place">
                    <div class="container">

                        <div class="heading">популярные <br/>направления</div>

                        <ul class="place__list">
                            <li>
                                <a href="#" class="place__tile place__tile_large">
                                    <div class="place__tile_image">
                                        <div class="place__tile_inner">
                                            <img src="images/direction__01.jpg" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <span class="place__tile_name">Дайвинг тур от 150$</span>
                                    <div class="place__tile_title">ДАЙВИНГ -<br/>НЕЗАБЫВАЕМОЕ ПОГРУЖЕНИЕ ПОД ВОДУ</div>
                                    <div class="place__tile_tags">Испания | Италия | Франция </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <div class="place__tile_wrap">
                                            <img src="images/direction__02.jpg" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <span class="place__tile_name">Винный тур от 150$</span>
                                    <div class="place__tile_text"><span>Попробуйте изысканные вина</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <img src="images/direction__03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="place__tile_name">Шопинг тур от 130$</span>
                                    <div class="place__tile_text"><span>Лучший шопинг в моей жизни, хочу еще!</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <img src="images/direction__04.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="place__tile_name">Гастрономический тур от 150$</span>
                                    <div class="place__tile_text"><span>Попробуйте изысканные блюда</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <img src="images/direction__05.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="place__tile_name">Бизнес тур от 150$</span>
                                    <div class="place__tile_text"><span>Организуем бизнес тур</span></div>
                                </a>
                            </li>
                        </ul>

                        <div class="place__quote">
                            <span>“</span>
                            <span>Воспоминания<br/>останутся на долго</span>
                            <span>“</span>
                            <div class="place__quote_author">worldbestguide.com</div>
                        </div>

                        <ul class="place__row">

                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <div class="place__tile_wrap">
                                            <img src="images/direction__06.jpg" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <span class="place__tile_name">Водный тур, Рафтинг от 130$ </span>
                                    <div class="place__tile_text"><span>Эмоции зашкаливают</span></div>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <div class="place__tile_wrap">
                                            <img src="images/direction__07.jpg" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <span class="place__tile_name">Пешеходная экскурсия от 130$</span>
                                    <div class="place__tile_text"><span>Пешеходная экскурсия</span></div>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="place__tile">
                                    <div class="place__tile_image">
                                        <div class="place__tile_wrap">
                                            <img src="images/direction__08.jpg" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <span class="place__tile_name">Приключения и экстрим от 150$</span>
                                    <div class="place__tile_text"><span>Приключения ждут Вас</span></div>
                                </a>
                            </li>

                        </ul>

                        <div class="text-center">
                            <a href="#" class="btn_md">Показать все туры</a>
                        </div>

                    </div>
                </section>

                <section class="intro">
                    <div class="container">
                        <div class="heading">наш блог</div>
                        <div class="intro__row">

                            <div class="intro__gallery">
                                <div class="intro__gallery_lg">
                                    <img src="images/intro__image_lg.jpg" class="img-fluid" alt="">
                                    <span class="intro__gallery_name">БЛОГ > ПАРИЖ</span>
                                    <div class="intro__gallery_icon"></div>
                                </div>
                                <ul class="intro__gallery_thumbs">
                                    <li>
                                        <div class="intro_item">
                                            <img src="images/intro__image_01.jpg" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="intro_item">
                                            <img src="images/intro__image_02.jpg" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="intro_item">
                                            <img src="images/intro__image_03.jpg" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="intro_item">
                                            <img src="images/intro__image_04.jpg" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="intro__blog">
                                <a href="#" class="intro__blog_quote">ЭТО БЫЛА УДИВИТЕЛЬНАЯ ПОЕЗДКА, КОТОРАЯ ИЗМЕНИЛА МОЙ ВЗГЛЯД НА ФРАНЦИЮ</a>
                                <div class="intro__blog_text">
                                    <p>Мы приехали в самый жаркий месяц и мы сразу окунились в жизнь города, который со старых времен приподносит уникальные. Большое спасибо нашему гиду <a href="#">Александру Пушкову</a>, который позволил нам окунуться в мир Парижа, который сделал наш день незабываемым.</p>
                                    <p>Мы приехали в самый жаркий месяц и мы сразу окунились в жизнь города, который со старых времен приподносит уникальные. Большое спасибо нашему гиду <a href="#">Александру Пушкову</a>,, который ...</p>
                                    <div class="text-right">
                                        <a class="intro__blog_next" href="#"><span>ЧИТАТЬ ИСТОРИЮ ДО КОНЦА</span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="intro__list">

                                <div class="intro__post intro__post_first">
                                    <a href="#" class="intro__post_image">
                                        <img src="images/intro__post_image_01.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="intro__post_text">
                                        <a href="#">20.11 <strong>Как мы прошли квест по кафе и ресторанам в Париже</strong></a>
                                    </div>
                                </div>

                                <div class="intro__post ">
                                    <a href="#" class="intro__post_image">
                                        <img src="images/intro__post_image_02.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="intro__post_text">
                                        <a href="#">20.11 <strong>Как мы прошли квест по кафе и ресторанам в Париже</strong></a>
                                    </div>
                                </div>

                                <div class="intro__post">
                                    <a href="#" class="intro__post_image">
                                        <img src="images/intro__post_image_03.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="intro__post_text">
                                        <a href="#">20.11 <strong>Как мы прошли квест по кафе и ресторанам в Париже</strong></a>
                                    </div>
                                </div>

                                <div class="intro__post">
                                    <a href="#" class="intro__post_image">
                                        <img src="images/intro__post_image_04.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="intro__post_text">
                                        <a href="#">20.11 <strong>Как мы прошли квест по кафе и ресторанам в Париже</strong></a>
                                    </div>
                                </div>

                                <div class="intro__post">
                                    <a href="#" class="intro__post_image">
                                        <img src="images/intro__post_image_05.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="intro__post_text">
                                        <a href="#">20.11 <strong>Как мы прошли квест по кафе и ресторанам в Париже</strong></a>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <a class="intro__list_next" href="#"></a>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

            <div class="page_home__elem elem_01">
                <div class="page_home__item">
                    <img src="img/homepage/home__01.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_02">
                <div class="page_home__item">
                    <img src="img/homepage/home__02.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_03">
                <div class="page_home__item">
                    <img src="img/homepage/home__03.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_04">
                <div class="page_home__item">
                    <img src="img/homepage/home__04.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_05"></div>
            <div class="page_home__elem elem_06">
                <div class="page_home__item">
                    <img src="img/homepage/home__06.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_07">
                <div class="page_home__item">
                    <img src="img/homepage/home__07.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_08">
                <div class="page_home__item">
                    <img src="img/homepage/home__08.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_09">
                <div class="page_home__item">
                    <img src="img/homepage/home__09.svg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="page_home__elem elem_10">
                <div class="page_home__item">
                    <img src="img/homepage/home__10.svg" class="img-fluid" alt="">
                </div>
            </div>

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>

</html>
