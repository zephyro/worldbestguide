<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="page_top">

            </div>

            <section class="main">
                <div class="container">

                    <p>
                        <a href="#enter" class="btn_modal">Вход</a>
                    </p>

                    <p>
                        <a href="#reg" class="btn_modal">Регистрация</a>
                    </p>

                    <p>
                        <a href="#recovery" class="btn_modal">Восстановление пароля</a>
                    </p>

                    <p>
                        <a href="#new_pass" class="btn_modal">Новый пароль</a>
                    </p>

                    <p>
                        <a href="#successfully" class="btn_modal">Ваш новый пароль успешно сохранен</a>
                    </p>

                    <p>
                        <a href="#successfully_email" class="btn_modal">Отправлена ссылка</a>
                    </p>

                    <p>
                        <a href="#welcome" class="btn_modal">Добро пожаловать</a>
                    </p>
                    <p>
                        <a href="#recovery_info" class="btn_modal">Восстановление пароля</a>
                    </p>
                    <p>
                        <a href="#access" class="btn_modal">Доступ к странице общения</a>
                    </p>
                    <p>
                        <a href="#balance" class="btn_modal">Пополнить баланс аккаунта</a>
                    </p>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <!-- Ваш новый пароль успешно сохранен -->
        <div class="hide">
            <a href="#successfully" class="open_new_pas btn_modal"></a>
            <div class="modal" id="successfully">
                <a href="#" class="modal__prev">
                    <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
                </a>
                <div class="modal__main">
                    <div class="modal__wrap">

                        <div class="modal__info">

                            <div class="modal__info_success">
                                <img src="img/img__successfully.svg" class="img-fluid" alt="">
                            </div>
                            <div class="modal__info_title">Успешно</div>
                            <div class="modal__info_text mb_25">Ваш новый пароль успешно сохранен</div>
                            <div class="modal__info_button">
                                <a href="#" class="btn_main btn_long">Войти на сайт</a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- -->

        <!-- Отправлена ссылка -->
        <div class="hide">
            <a href="#successfully_email" class="open_new_pas btn_modal"></a>
            <div class="modal" id="successfully_email">
                <a href="#" class="modal__prev">
                    <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
                </a>
                <div class="modal__main">
                    <div class="modal__wrap">

                        <div class="modal__info">

                            <div class="modal__info_success">
                                <img src="img/img__successfully.svg" class="img-fluid" alt="">
                            </div>
                            <div class="modal__info_title">Успешно</div>
                            <div class="modal__info_text mb_25">НА ВАШ E-MAIL ОТПРАВЛЕНА ССЫЛКА <br/>ДЛЯ ВОССТАНОВЛЕНИЯ ПАРОЛЯ</div>
                            <div class="modal__info_button">
                                <a href="#" class="btn_main btn_long">на главную</a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- -->

        <!-- Добро пожаловать -->
        <div class="hide">
            <a href="#welcome" class="open_welcome btn_modal"></a>
            <div class="modal" id="welcome">
                <a href="#" class="modal__prev">
                    <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
                </a>
                <div class="modal__main">
                    <div class="modal__wrap">

                        <div class="modal__info">

                            <div class="modal__info_auto">
                                <img src="img/img__auto.svg" class="img-fluid" alt="">
                            </div>
                            <div class="modal__info_title">Добро пожаловать!</div>
                            <div class="modal__info_subtitle">Здравствуйте,</div>
                            <div class="modal__info_text mb_25">Вы зарегистрировались на сайте  <a href="#">WorldBestGuide.com</a><br/>
                                Чтобы активировать аккаунт и начать работу в системе нажмите кнопку</div>
                            <div class="modal__info_button">
                                <a href="#" class="btn_main btn_long">активировать</a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- -->

        <!-- Восстановление пароля -->
        <div class="hide">
            <a href="#recovery_info" class="open_recovery_info btn_modal"></a>
            <div class="modal" id="recovery_info">
                <a href="#" class="modal__prev">
                    <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
                </a>
                <div class="modal__main">
                    <div class="modal__wrap">

                        <div class="modal__info">

                            <div class="modal__info_key">
                                <img src="img/img__key.svg" class="img-fluid" alt="">
                            </div>
                            <div class="modal__info_title">восстановление пароля</div>
                            <div class="modal__info_subtitle">Здравствуйте,</div>
                            <div class="modal__info_text mb_25">
                                Вы или кто-то другой запросил восстановление пароля  на сайте  <a href="#">WorldBestGuide.com</a>
                                Чтобы установить новый пароль  и начать работу в системе нажмите кнопку

                            </div>
                            <div class="modal__info_button">
                                <a href="#" class="btn_main btn_long">Новый пароль</a>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal__footer">С уважением, команда WorldBestGuide.com</div>
            </div>

        </div>
        <!-- -->

        <!-- Доступ к странице общения -->
        <div class="hide">
            <a href="#access" class="open_access btn_modal"></a>
            <div class="modal" id="access">
                <a href="#" class="modal__prev">
                    <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
                </a>
                <div class="modal__main">
                    <div class="modal__wrap">

                        <div class="modal__heading">
                            <div class="modal__heading_title">ДОСТУП К СТРАНИЦЕ ОБЩЕНИЯ</div>
                        </div>

                        <div class="modal__box">
                            <div class="modal__box_text">
                                ПРЕДВАРИТЕЛЬНАЯ СТОИМОСТЬ ЗАКАЗА<br/>СОСТАВЛЯЕТ <strong>180$</strong>
                            </div>
                        </div>

                        <div class="modal__info text-center">
                            <div class="modal__info_text">
                                <p>Для того, чтобы получить доступ к общению с клиентом Вам необходимо зарезервировать <strong>$25.</strong></p>
                                <p>В случае отстуствия информации о клиенте в переписке и передачи информации. Вы можете запросить возврат средств на Ваш счет в системе</p>
                                <p class="mb_20">Ваш текущий баланс: $15 <a href="#">пополнить</a></p>
                            </div>
                            <a href="#" class="btn">Зарезервировать $25</a>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- -->

        <!-- Пополнить баланс аккаунта -->
        <div class="hide">
            <a href="#balance" class="open_balance btn_modal"></a>
            <div class="modal" id="balance">
                <a href="#" class="modal__prev">
                    <img src="img/icon__angle_left.svg" class="img-fluid" alt="">
                </a>
                <div class="modal__main">
                    <div class="modal__wrap">

                        <div class="modal__heading">
                            <div class="modal__heading_title">ДОСТУП К СТРАНИЦЕ ОБЩЕНИЯ</div>
                        </div>

                        <div class="modal__box">
                            <div class="modal__box_title">Сумма, $</div>
                            <div class="modal__box_form">
                                <div class="form_group modal__box_input">
                                    <input type="text" class="form_control" placeholder="" value="159">
                                </div>
                                <div class="form_group mb_40">
                                    <select class="form_select">
                                        <option value="0">Выберите способ оплаты</option>
                                        <option value="1">PayPal</option>
                                        <option value="2">Яндкекс Деньги</option>
                                        <option value="3">Credit Card</option>
                                    </select>
                                </div>
                                <div class="modal__box_button">
                                    <button type="submit" class="btn">ПОПОЛНИТЬ БАЛАНС</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- -->

    </body>
</html>
