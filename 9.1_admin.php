<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_one" style="background-image: url('img/header__bg_3.jpg');">
            <div class="container">
                <h1>
                    <span>все</span>
                    <br>
                    <span>заказы</span>
                </h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row">
                <div class="main_sidebar">

                    <!-- User sidebar -->
                    <div class="user_sidebar">

                        <div class="sidebar_photo">
                            <div class="sidebar_photo__item">
                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                <span>aLEX PUSHKOV</span>
                            </div>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Админ</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">Заказы <span class="color_green">56</span> | <span class="color_purple">56</span></a></li>
                                <li><a href="#">Транзакции</a></li>
                                <li><a href="#">Статьи</a></li>
                                <li><a href="#">Пользователи</a></li>
                                <li><a href="#">Гиды <span class="color_green">56</span> | <span class="color_purple">56</span></a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Основное</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">МОИ Заказы (5)</a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Дополнительно</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">МОИ Статьи (5)</a></li>
                                <li><a href="#">Добавить статью</a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Я Гид</div>
                            <ul class="sidenav__menu">
                                <li><a href="#">Мой баланс $345 | <span class="color_purple">$112</span></a></li>
                                <li><a href="#">МОИ ЗАКАЗЫ (5) <span class="sidenav__menu_value">+3</span></a></li>
                                <li><a href="#">МОИ услуги (5)</a></li>
                                <li><a href="#">МОИ туры (15)</a></li>
                                <li><a href="#" class="sidenav__menu_exit">ВЫХОД</a></li>
                            </ul>
                        </div>

                    </div>
                    <!-- -->

                </div>
                <div class="main_content">

                    <h3 class="color_red">ЗАПРОС НА ВОЗВРАТ СРЕДСТВ</h3>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th>название</th>
                                <th>дата</th>
                                <th class="text-center">Цена заявки</th>
                                <th class="text-center">сумма</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                        </table>
                    </div>

                    <h3 class="color_green">Заказы</h3>

                    <div class="table_responsive mb_40">
                        <table class="table">

                            <tr>
                                <th>название</th>
                                <th>дата</th>
                                <th class="text-center">Цена заявки</th>
                                <th class="text-center">сумма</th>
                                <th class="text-center"></th>
                            </tr>

                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>

                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>

                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>
                            <tr>
                                <td><div class="text_name">Главные сокровища Лувра за 2 часа</div></td>
                                <td>15.12.2018, 11:23</td>
                                <td class="text-center">~$56</td>
                                <td class="text-center">~$356</td>
                                <td class="text-center"><a class="text-uppercase" href="#"><a href="#" class="blue_link">Ответить</a></a></td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
