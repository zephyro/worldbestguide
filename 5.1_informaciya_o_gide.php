<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="page_heading_wrap">

                <div class="page_heading page_heading_one" style="background-image: url('img/guide_info__bg.jpg');">
                    <div class="container">
                        <h1>
                            <span>Александр пушков</span>
                        </h1>
                    </div>
                </div>
            </div>


            <section class="main">
                <div class="container">
                    <div class="main_row">
                        <div class="main_sidebar">

                            <!-- User sidebar -->
                            <?php include('inc/user_sidebar.inc.php') ?>
                            <!-- -->

                        </div>
                        <div class="main_content">

                            <div class="guide_heading">
                                <div class="guide_heading__text">
                                    <h3>Информация о гиде</h3>
                                </div>
                                <div class="guide_heading__action">
                                    <a href="#" class="btn">НАПИСАТЬ ГИДУ</a>
                                </div>
                            </div>

                            <div class="content_block">
                                <p>Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </p>

                                <p>Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </p>

                                <p>Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </p>

                            </div>

                            <div class="content_block">
                                <h3>ЯЗЫКИ</h3>
                                <p>RU | EN | FR</p>
                            </div>

                            <h3>ТУРЫ ГИДА</h3>

                            <div class="goods__row">

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_01.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ЧЕХИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_03.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ВЕНЕСУЭЛЛА</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Александра с очень дли</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_01.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">РОССИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ЧЕХИЯ</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__name">Татьяна</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="goods__elem">
                                    <div class="goods">
                                        <div class="goods__image">
                                            <a href="#" class="goods__image_wrap">
                                                <img src="images/guide_user_03.png" class="img-fluid" alt="">
                                            </a>
                                            <div class="goods__tags">
                                                <a href="#">ВЕНЕСУЭЛЛА</a>
                                            </div>
                                            <div class="goods__rating">
                                                <div class="goods__rating_star">
                                                    <div class="raty" data-score="5" data-readOnly="true"></div>
                                                </div>
                                                <span class="goods__rating_text">59 отзывов</span>
                                            </div>
                                        </div>
                                        <a href="#" class="goods__content">
                                            <div class="goods__user">Александра с очень дли</div>
                                            <div class="goods__lang">
                                                <div class="goods__lang_elems">
                                                    <span>ru</span> | <span>en</span>
                                                </div>
                                            </div>
                                            <div class="goods__text">Профессиональный перевод (устный - последовательный, шушотаж, а также письменный) с/на английский, французский, русский языки на деловых встречах, конференциях, выставках, переговорах, презентациях, пресс-конференциях и других деловых мероприятиях. Предлагаю услуги сопровождения для частных лиц в </div>
                                            <div class="goods__data">
                                                <div class="goods__data_item">
                                                    <strong>14</strong>
                                                    <span>туров</span>
                                                </div>
                                                <div class="goods__data_item">
                                                    <strong>59</strong>
                                                    <span>отзывов</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
