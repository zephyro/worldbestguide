<!doctype html>
<html class="no-js" lang="ru">

<!-- Head -->
<?php include('inc/head.inc.php') ?>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header_auth.inc.php') ?>
    <!-- -->

    <div class="page_heading_wrap">

        <div class="page_heading page_heading_two" style="background-image: url('img/header__bg_4.jpg');">
            <div class="container">
                <h1><span>ОФОРМЛЕНИЕ ЗАКАЗА</span></h1>
            </div>
        </div>
    </div>


    <section class="main">
        <div class="container">
            <div class="main_row main_row_wrap">

                <div class="main_sidebar">

                    <div class="user_sidebar">

                        <div class="sidebar_photo">
                            <div class="sidebar_photo__item">
                                <img src="images/guide_user_02.png" class="img-fluid" alt="">
                                <span>aLEX PUSHKOV</span>
                            </div>
                        </div>


                        <div class="sidenav">
                            <ul class="sidenav__menu">
                                <li><a href="#">МОИ ЗАКАЗЫ (5) <span class="sidenav__menu_value">+3</span></a></li>
                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Дополнительно</div>

                            <ul class="sidenav__menu">
                                <li><a href="#">Добавить статью</a></li>
                                <li><a href="#">МОИ СТАТЬИ</a></li>

                            </ul>
                        </div>

                        <div class="sidenav">
                            <div class="sidenav__title">Настройки</div>

                            <ul class="sidenav__menu">
                                <li><a href="#">Мои настройки</a></li>
                                <li><a href="#" class="sidenav__menu_exit">ВЫХОД</a></li>
                            </ul>
                        </div>

                    </div>


                </div>

                <div class="main_content">
                    <div class="main_content__row">

                        <div class="main_center">
                            <h3>ДАТА ПОЕЗДКИ</h3>
                            <div class="form_group_date mb_40">
                                <div class="row">
                                    <div class="col col-xs-7 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Дата</label>
                                            <input class="form_control form_date" type="text" name="name" placeholder="" value="27.07.2019">
                                        </div>
                                    </div>
                                    <div class="col col-xs-5 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Время</label>
                                            <select class="form_select form_time">
                                                <option value="00:00">08:00</option>
                                                <option value="00:00">09:00</option>
                                                <option value="00:00">10:00</option>
                                                <option value="00:00">11:00</option>
                                                <option value="00:00">12:00</option>
                                                <option value="00:00">13:00</option>
                                                <option value="00:00">14:00</option>
                                                <option value="00:00">1500</option>
                                                <option value="00:00">16:00</option>
                                                <option value="00:00">17:00</option>
                                                <option value="00:00">18:00</option>
                                                <option value="00:00">19:00</option>
                                                <option value="00:00">20:00</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3>СКОЛЬКО ВАС БУДЕТ</h3>
                            <div class="form_group mb_40">
                                <div class="form_user">
                                    <div class="form_user__elem">
                                        <label class="form_label">Взрослых</label>
                                        <select class="form_select">
                                            <option value="">1</option>
                                            <option value="">2</option>
                                            <option value="">3</option>
                                        </select>
                                    </div>
                                    <div class="form_user__elem">
                                        <label class="form_label">Детей</label>
                                        <select class="form_select">
                                            <option value="">0</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                            <option value="">3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <h3>ВЫБЕРИТЕ ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ</h3>
                            <div class="row mb_40">
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$150 - Трансфер с аэропорта</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$50 - Услуги переводчика (en|ru|fr)</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$150 - Трансфер с аэропорта</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$50 - Услуги переводчика (en|ru|fr)</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$150 - Трансфер с аэропорта</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$50 - Услуги переводчика (en|ru|fr)</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$150 - Трансфер с аэропорта</span>
                                    </label>
                                </div>
                                <div class="col col-xs-12 col-lg-6 col-xl-5 col-gutter-lr mb_10">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="">
                                        <span>$50 - Услуги переводчика (en|ru|fr)</span>
                                    </label>
                                </div>
                            </div>

                            <h3>ЭКСКУРСИОННЫЙ ТУР</h3>

                            <div class="form_label">Все туры гида</div>
                            <div class="row mb_40">
                                <div class="col col-xs-12 col-lg-5 col-xl-4 col-gutter-lr">
                                    <select class="form_select">
                                        <option value="">По Версалю не спеша</option>
                                        <option value="">По Версалю не спеша</option>
                                        <option value="">По Версалю не спеша</option>
                                        <option value="">По Версалю не спеша</option>
                                    </select>
                                </div>
                                <div class="col col-xs-12 col-lg-7 col-xl-8 col-gutter-lr center_box">
                                    <a href="#">просмотреть все туры гида</a>
                                </div>
                            </div>

                            <h3>ОСТАВИТЬ КОММЕНТАРИЙ ИЛИ ЗАДАТЬ ВОПРОС</h3>
                            <div class="form_group">
                                <div class="form_label">Напишите сообщение</div>
                                <textarea class="form_control" name="name" placeholder="" rows="7"></textarea>
                            </div>

                        </div>

                        <div class="main_right">
                            <div class="side">
                                <h3>МОЙ ЗАКАЗ</h3>
                                <h6>эКСКУРСИОННЫЙ ТУР</h6>
                                <p><a class="color_blue text-uppercase" href="#">По Версалю не спеша</a></p>
                                <ul class="side__list mb_20">
                                    <li>Дата: <strong>15.11.2018 в 09:30</strong></li>
                                    <li> Взрослых: <strong>5</strong></li>
                                    <li>Детей: <strong>0</strong></li>
                                    <li> Длительность: <strong>2 часа</strong></li>
                                </ul>
                                <h6>дополнительные услуги</h6>
                                <p class="mb_20">$35 - трансфер с аэропорта</p>

                                <div class="side_form_group">
                                    <h6>Быстрая регистрация</h6>
                                    <div class="form_group">
                                        <label class="form_label">Ваше имя<sup>*</sup></label>
                                        <input class="form_control" type="text" name="name" placeholder="Александр Пушков" value="">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">E-mail<sup>*</sup></label>
                                        <input class="form_control" type="text" name="name" placeholder="info@hix.one" value="">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">Телефон<sup>*</sup></label>
                                        <input class="form_control" type="text" name="name" placeholder="+375297507755" value="">
                                    </div>
                                </div>

                                <div class="form_group">
                                    <strong class="text-uppercase">ИТОГО ~ $250</strong>
                                </div>
                                <div class="form_group">
                                    <div class="white_box">Оплата на месте или по договоренности с гидом</div>
                                </div>

                                <a class="btn btn_long" href="#">начать чат с гидом</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- include summernote css/js -->
<link href="js/vendor/summernote/summernote-lite.css" rel="stylesheet">
<script src="js/vendor/summernote/summernote-lite.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200
        });
    });
</script>

</body>
</html>
